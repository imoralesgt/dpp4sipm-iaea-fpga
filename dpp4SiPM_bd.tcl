
################################################################
# This is a generated script based on design: design_1
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2019.1
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source design_1_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7a35tcsg325-1
   set_property BOARD_PART iaea.org:dpp4sipm:part0:1.0 [current_project]
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name design_1

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

set bCheckIPsPassed 1
##################################################################
# CHECK IPs
##################################################################
set bCheckIPs 1
if { $bCheckIPs == 1 } {
   set list_check_ips "\ 
xilinx.com:ip:clk_wiz:6.0\
xilinx.com:ip:system_ila:1.1\
xilinx.com:user:CDCC:1.0\
iaea.org:ip:ddr2sdr:1.0\
xilinx.com:ip:axi_gpio:2.0\
xilinx.com:ip:axi_quad_spi:3.2\
xilinx.com:ip:axi_uartlite:2.0\
xilinx.com:ip:axi_intc:4.1\
xilinx.com:ip:mdm:3.2\
xilinx.com:ip:microblaze:11.0\
xilinx.com:ip:proc_sys_reset:5.0\
xilinx.com:ip:xlconstant:1.1\
xilinx.com:user:ip_dbg_pha:1.0\
xilinx.com:user:ip_timers:3.0\
iaea.org:user:ip_dbg_pulse_cond_fast:1.0\
xilinx.com:user:ip_dc_stabilizer:2.0\
IAEA:IP_Shaper:ip_shaper:4.1\
iaea.org:interface:ip_dbg_pulse_cond_slow:1.0\
xilinx.com:user:invert_and_offset:1.0\
iaea.org:user:ip_dbg_invert_and_offset:1.0\
xilinx.com:user:ip_mux16_2_if:1.0\
User_Company:SysGen:ip_scope:1.2\
xilinx.com:ip:lmb_bram_if_cntlr:4.0\
xilinx.com:ip:lmb_v10:3.0\
xilinx.com:ip:blk_mem_gen:8.4\
xilinx.com:ip:util_reduced_logic:2.0\
xilinx.com:ip:util_vector_logic:2.0\
xilinx.com:ip:xlconcat:2.1\
xilinx.com:ip:axi_bram_ctrl:4.1\
xilinx.com:user:bram_incr:1.0\
xilinx.com:user:ShiftRegisterControl:1.0\
xilinx.com:ip:c_shift_ram:12.0\
IAEA:user:ip_peakdetector:1.0\
xilinx.com:user:ip_pur:2.0\
"

   set list_ips_missing ""
   common::send_msg_id "BD_TCL-006" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

   foreach ip_vlnv $list_check_ips {
      set ip_obj [get_ipdefs -all $ip_vlnv]
      if { $ip_obj eq "" } {
         lappend list_ips_missing $ip_vlnv
      }
   }

   if { $list_ips_missing ne "" } {
      catch {common::send_msg_id "BD_TCL-115" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
      set bCheckIPsPassed 0
   }

}

if { $bCheckIPsPassed != 1 } {
  common::send_msg_id "BD_TCL-1003" "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 3
}

##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: pur
proc create_hier_cell_pur { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_pur() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S16_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S17_AXI


  # Create pins
  create_bd_pin -dir I -type clk clk_cpu
  create_bd_pin -dir I clk_cpu_aresetn
  create_bd_pin -dir I -type clk clk_dpp
  create_bd_pin -dir O -from 1 -to 0 lt_dir
  create_bd_pin -dir O -from 0 -to 0 -type data peak_amp_rdy
  create_bd_pin -dir O -from 0 -to 0 rejectn
  create_bd_pin -dir I -from 15 -to 0 -type data x

  # Create instance: ip_peakdetector_0, and set properties
  set ip_peakdetector_0 [ create_bd_cell -type ip -vlnv IAEA:user:ip_peakdetector:1.0 ip_peakdetector_0 ]

  # Create instance: ip_pur_0, and set properties
  set ip_pur_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:ip_pur:2.0 ip_pur_0 ]
  set_property -dict [ list \
   CONFIG.C_S00_PUR_COUNTER_WIDTH {8} \
 ] $ip_pur_0

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins S16_AXI] [get_bd_intf_pins ip_peakdetector_0/axibusdomain_s_axi]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins S17_AXI] [get_bd_intf_pins ip_pur_0/S00_AXI]

  # Create port connections
  connect_bd_net -net clk_cpu_1 [get_bd_pins clk_cpu] [get_bd_pins ip_peakdetector_0/axibusdomain_clk] [get_bd_pins ip_pur_0/s00_axi_aclk]
  connect_bd_net -net clk_cpu_aresetn_1 [get_bd_pins clk_cpu_aresetn] [get_bd_pins ip_peakdetector_0/axibusdomain_aresetn] [get_bd_pins ip_pur_0/resetn] [get_bd_pins ip_pur_0/s00_axi_aresetn]
  connect_bd_net -net clk_dpp_1 [get_bd_pins clk_dpp] [get_bd_pins ip_peakdetector_0/signaldomain_clk] [get_bd_pins ip_pur_0/clk]
  connect_bd_net -net ip_peakdetector_0_peak_amp_rdy [get_bd_pins peak_amp_rdy] [get_bd_pins ip_peakdetector_0/peak_amp_rdy] [get_bd_pins ip_pur_0/amp_rdy]
  connect_bd_net -net ip_pur_0_lt_dir [get_bd_pins lt_dir] [get_bd_pins ip_pur_0/lt_dir]
  connect_bd_net -net ip_pur_0_rejectn [get_bd_pins rejectn] [get_bd_pins ip_pur_0/rejectn]
  connect_bd_net -net x_1 [get_bd_pins x] [get_bd_pins ip_peakdetector_0/x]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: pkd
proc create_hier_cell_pkd { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_pkd() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S11_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S12_AXI


  # Create pins
  create_bd_pin -dir I -type clk clk_cpu
  create_bd_pin -dir I -type rst clk_cpu_aresetn
  create_bd_pin -dir I -type clk clk_dpp
  create_bd_pin -dir O -from 15 -to 0 -type data peak_amp
  create_bd_pin -dir O -from 0 -to 0 -type data peak_amp_rdy
  create_bd_pin -dir I -from 15 -to 0 -type data x

  # Create instance: ShiftRegisterControl_0, and set properties
  set ShiftRegisterControl_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:ShiftRegisterControl:1.0 ShiftRegisterControl_0 ]
  set_property -dict [ list \
   CONFIG.SHIFT_WIDTH {10} \
 ] $ShiftRegisterControl_0

  # Create instance: c_shift_ram_0, and set properties
  set c_shift_ram_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_0 ]
  set_property -dict [ list \
   CONFIG.Depth {1024} \
   CONFIG.ShiftRegType {Variable_Length_Lossless} \
 ] $c_shift_ram_0

  # Create instance: ip_peakdetector_0, and set properties
  set ip_peakdetector_0 [ create_bd_cell -type ip -vlnv IAEA:user:ip_peakdetector:1.0 ip_peakdetector_0 ]

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins S11_AXI] [get_bd_intf_pins ShiftRegisterControl_0/S00_AXI]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins S12_AXI] [get_bd_intf_pins ip_peakdetector_0/axibusdomain_s_axi]

  # Create port connections
  connect_bd_net -net D_1 [get_bd_pins x] [get_bd_pins c_shift_ram_0/D]
  connect_bd_net -net ShiftRegisterControl_0_shift_value [get_bd_pins ShiftRegisterControl_0/shift_value] [get_bd_pins c_shift_ram_0/A]
  connect_bd_net -net c_shift_ram_0_Q [get_bd_pins c_shift_ram_0/Q] [get_bd_pins ip_peakdetector_0/x]
  connect_bd_net -net clk_1 [get_bd_pins clk_dpp] [get_bd_pins c_shift_ram_0/CLK] [get_bd_pins ip_peakdetector_0/signaldomain_clk]
  connect_bd_net -net clk_out1_1 [get_bd_pins clk_cpu] [get_bd_pins ShiftRegisterControl_0/s00_axi_aclk] [get_bd_pins ip_peakdetector_0/axibusdomain_clk]
  connect_bd_net -net ip_peakdetector_0_peak_amp [get_bd_pins peak_amp] [get_bd_pins ip_peakdetector_0/peak_amp]
  connect_bd_net -net ip_peakdetector_0_peak_amp_rdy [get_bd_pins peak_amp_rdy] [get_bd_pins ip_peakdetector_0/peak_amp_rdy]
  connect_bd_net -net peripheral_aresetn_1 [get_bd_pins clk_cpu_aresetn] [get_bd_pins ShiftRegisterControl_0/s00_axi_aresetn] [get_bd_pins ip_peakdetector_0/axibusdomain_aresetn]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: mca
proc create_hier_cell_mca { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_mca() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S10_AXI


  # Create pins
  create_bd_pin -dir I -type clk clk_cpu
  create_bd_pin -dir I -type rst clk_cpu_aresetn
  create_bd_pin -dir I clk_dpp
  create_bd_pin -dir I -from 15 -to 0 -type data peak_amp
  create_bd_pin -dir I peak_amp_rdy
  create_bd_pin -dir I segment

  # Create instance: axi_bram_ctrl_0, and set properties
  set axi_bram_ctrl_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.1 axi_bram_ctrl_0 ]
  set_property -dict [ list \
   CONFIG.SINGLE_PORT_BRAM {1} \
 ] $axi_bram_ctrl_0

  # Create instance: blk_mem_gen_0, and set properties
  set blk_mem_gen_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 blk_mem_gen_0 ]
  set_property -dict [ list \
   CONFIG.EN_SAFETY_CKT {false} \
   CONFIG.Enable_B {Use_ENB_Pin} \
   CONFIG.Memory_Type {True_Dual_Port_RAM} \
   CONFIG.Port_B_Clock {100} \
   CONFIG.Port_B_Enable_Rate {100} \
   CONFIG.Port_B_Write_Rate {50} \
   CONFIG.Use_RSTB_Pin {true} \
 ] $blk_mem_gen_0

  # Create instance: bram_incr_0, and set properties
  set bram_incr_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:bram_incr:1.0 bram_incr_0 ]

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins S10_AXI] [get_bd_intf_pins axi_bram_ctrl_0/S_AXI]
  connect_bd_intf_net -intf_net axi_bram_ctrl_0_BRAM_PORTA [get_bd_intf_pins axi_bram_ctrl_0/BRAM_PORTA] [get_bd_intf_pins blk_mem_gen_0/BRAM_PORTA]
  connect_bd_intf_net -intf_net bram_incr_0_BRAM_PORTB [get_bd_intf_pins blk_mem_gen_0/BRAM_PORTB] [get_bd_intf_pins bram_incr_0/BRAM_PORTB]

  # Create port connections
  connect_bd_net -net clk_1 [get_bd_pins clk_dpp] [get_bd_pins bram_incr_0/clk]
  connect_bd_net -net clk_out1_1 [get_bd_pins clk_cpu] [get_bd_pins axi_bram_ctrl_0/s_axi_aclk]
  connect_bd_net -net peak_amp_1 [get_bd_pins peak_amp] [get_bd_pins bram_incr_0/peak_amp]
  connect_bd_net -net peak_amp_rdy_1 [get_bd_pins peak_amp_rdy] [get_bd_pins bram_incr_0/peak_amp_rdy]
  connect_bd_net -net peripheral_aresetn_1 [get_bd_pins clk_cpu_aresetn] [get_bd_pins axi_bram_ctrl_0/s_axi_aresetn] [get_bd_pins bram_incr_0/rstn]
  connect_bd_net -net segment_1 [get_bd_pins segment] [get_bd_pins bram_incr_0/segment]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: glue_logic
proc create_hier_cell_glue_logic { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_glue_logic() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I -from 0 -to 0 In0
  create_bd_pin -dir I -from 0 -to 0 -type data In1
  create_bd_pin -dir I -from 0 -to 0 In2
  create_bd_pin -dir O Res
  create_bd_pin -dir O -from 0 -to 0 Res1

  # Create instance: util_reduced_logic_0, and set properties
  set util_reduced_logic_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_reduced_logic:2.0 util_reduced_logic_0 ]
  set_property -dict [ list \
   CONFIG.C_SIZE {3} \
 ] $util_reduced_logic_0

  # Create instance: util_vector_logic_0, and set properties
  set util_vector_logic_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_0 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {not} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_notgate.png} \
 ] $util_vector_logic_0

  # Create instance: xlconcat_0, and set properties
  set xlconcat_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_0 ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {3} \
 ] $xlconcat_0

  # Create port connections
  connect_bd_net -net In0_1 [get_bd_pins In0] [get_bd_pins xlconcat_0/In0]
  connect_bd_net -net In1_1 [get_bd_pins In1] [get_bd_pins xlconcat_0/In1]
  connect_bd_net -net In2_1 [get_bd_pins In2] [get_bd_pins util_vector_logic_0/Op1] [get_bd_pins xlconcat_0/In2]
  connect_bd_net -net util_reduced_logic_0_Res [get_bd_pins Res] [get_bd_pins util_reduced_logic_0/Res]
  connect_bd_net -net util_vector_logic_0_Res [get_bd_pins Res1] [get_bd_pins util_vector_logic_0/Res]
  connect_bd_net -net xlconcat_0_dout [get_bd_pins util_reduced_logic_0/Op1] [get_bd_pins xlconcat_0/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: microblaze_0_local_memory
proc create_hier_cell_microblaze_0_local_memory { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_microblaze_0_local_memory() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 DLMB

  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 ILMB


  # Create pins
  create_bd_pin -dir I -type clk LMB_Clk
  create_bd_pin -dir I -type rst SYS_Rst

  # Create instance: dlmb_bram_if_cntlr, and set properties
  set dlmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 dlmb_bram_if_cntlr ]
  set_property -dict [ list \
   CONFIG.C_ECC {0} \
 ] $dlmb_bram_if_cntlr

  # Create instance: dlmb_v10, and set properties
  set dlmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 dlmb_v10 ]

  # Create instance: ilmb_bram_if_cntlr, and set properties
  set ilmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 ilmb_bram_if_cntlr ]
  set_property -dict [ list \
   CONFIG.C_ECC {0} \
 ] $ilmb_bram_if_cntlr

  # Create instance: ilmb_v10, and set properties
  set ilmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 ilmb_v10 ]

  # Create instance: lmb_bram, and set properties
  set lmb_bram [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 lmb_bram ]
  set_property -dict [ list \
   CONFIG.Memory_Type {True_Dual_Port_RAM} \
   CONFIG.use_bram_block {BRAM_Controller} \
 ] $lmb_bram

  # Create interface connections
  connect_bd_intf_net -intf_net microblaze_0_dlmb [get_bd_intf_pins DLMB] [get_bd_intf_pins dlmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_0_dlmb_bus [get_bd_intf_pins dlmb_bram_if_cntlr/SLMB] [get_bd_intf_pins dlmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_0_dlmb_cntlr [get_bd_intf_pins dlmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTA]
  connect_bd_intf_net -intf_net microblaze_0_ilmb [get_bd_intf_pins ILMB] [get_bd_intf_pins ilmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_0_ilmb_bus [get_bd_intf_pins ilmb_bram_if_cntlr/SLMB] [get_bd_intf_pins ilmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_0_ilmb_cntlr [get_bd_intf_pins ilmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTB]

  # Create port connections
  connect_bd_net -net SYS_Rst_1 [get_bd_pins SYS_Rst] [get_bd_pins dlmb_bram_if_cntlr/LMB_Rst] [get_bd_pins dlmb_v10/SYS_Rst] [get_bd_pins ilmb_bram_if_cntlr/LMB_Rst] [get_bd_pins ilmb_v10/SYS_Rst]
  connect_bd_net -net microblaze_0_Clk [get_bd_pins LMB_Clk] [get_bd_pins dlmb_bram_if_cntlr/LMB_Clk] [get_bd_pins dlmb_v10/LMB_Clk] [get_bd_pins ilmb_bram_if_cntlr/LMB_Clk] [get_bd_pins ilmb_v10/LMB_Clk]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: scope
proc create_hier_cell_scope { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_scope() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S03_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S13_AXI

  create_bd_intf_pin -mode Slave -vlnv iaea.org:interface:dbg_invert_and_offset_rtl:1.0 dbg_invert_and_offset_s0

  create_bd_intf_pin -mode Slave -vlnv iaea.org:interface:dbg_pha_rtl:1.0 dbg_pha_s0

  create_bd_intf_pin -mode Slave -vlnv iaea.org:interface:dbg_pulse_cond_fast_rtl:1.0 dbg_pulse_cond_fast_s0

  create_bd_intf_pin -mode Slave -vlnv iaea.org:interface:dbg_pulse_cond_slow_rtl:1.0 dbg_pulse_cond_slow_s0


  # Create pins
  create_bd_pin -dir I -type clk clk_cpu
  create_bd_pin -dir I -type rst clk_cpu_aresetn
  create_bd_pin -dir I -type clk clk_dpp
  create_bd_pin -dir O -from 0 -to 0 -type intr full

  # Create instance: ip_mux16_2_if_0, and set properties
  set ip_mux16_2_if_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:ip_mux16_2_if:1.0 ip_mux16_2_if_0 ]

  # Create instance: ip_scope_0, and set properties
  set ip_scope_0 [ create_bd_cell -type ip -vlnv User_Company:SysGen:ip_scope:1.2 ip_scope_0 ]

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins S03_AXI] [get_bd_intf_pins ip_scope_0/axibusdomain_s_axi]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins S13_AXI] [get_bd_intf_pins ip_mux16_2_if_0/S00_AXI]
  connect_bd_intf_net -intf_net dbg_invert_and_offset_s0_1 [get_bd_intf_pins dbg_invert_and_offset_s0] [get_bd_intf_pins ip_mux16_2_if_0/dbg_invert_and_offset_s0]
  connect_bd_intf_net -intf_net dbg_pha_s0_1 [get_bd_intf_pins dbg_pha_s0] [get_bd_intf_pins ip_mux16_2_if_0/dbg_pha_s0]
  connect_bd_intf_net -intf_net dbg_pulse_cond_fast_s0_1 [get_bd_intf_pins dbg_pulse_cond_fast_s0] [get_bd_intf_pins ip_mux16_2_if_0/dbg_pulse_cond_fast_s0]
  connect_bd_intf_net -intf_net dbg_pulse_cond_slow_s0_1 [get_bd_intf_pins dbg_pulse_cond_slow_s0] [get_bd_intf_pins ip_mux16_2_if_0/dbg_pulse_cond_slow_s0]

  # Create port connections
  connect_bd_net -net clk_1 [get_bd_pins clk_dpp] [get_bd_pins ip_scope_0/signaldomain_clk]
  connect_bd_net -net ip_mux16_2_if_0_outp1 [get_bd_pins ip_mux16_2_if_0/outp1] [get_bd_pins ip_scope_0/ch1]
  connect_bd_net -net ip_mux16_2_if_0_outp2 [get_bd_pins ip_mux16_2_if_0/outp2] [get_bd_pins ip_scope_0/ch2]
  connect_bd_net -net ip_mux16_2_if_0_trig [get_bd_pins ip_mux16_2_if_0/trig] [get_bd_pins ip_scope_0/ch_trigger]
  connect_bd_net -net ip_scope_0_full [get_bd_pins full] [get_bd_pins ip_scope_0/full]
  connect_bd_net -net s00_axi_aclk_1 [get_bd_pins clk_cpu] [get_bd_pins ip_mux16_2_if_0/s00_axi_aclk] [get_bd_pins ip_scope_0/axibusdomain_clk]
  connect_bd_net -net s00_axi_aresetn_1 [get_bd_pins clk_cpu_aresetn] [get_bd_pins ip_mux16_2_if_0/s00_axi_aresetn] [get_bd_pins ip_scope_0/axibusdomain_aresetn]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: pulse_offseting
proc create_hier_cell_pulse_offseting { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_pulse_offseting() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S05_AXI

  create_bd_intf_pin -mode Master -vlnv iaea.org:interface:dbg_invert_and_offset_rtl:1.0 dbg_invert_and_offset_m0


  # Create pins
  create_bd_pin -dir I -from 13 -to 0 adc_data
  create_bd_pin -dir I -type clk clk_cpu
  create_bd_pin -dir I -type rst clk_cpu_aresetn
  create_bd_pin -dir I clk_dpp
  create_bd_pin -dir O -from 15 -to 0 y

  # Create instance: invert_and_offset_0, and set properties
  set invert_and_offset_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:invert_and_offset:1.0 invert_and_offset_0 ]

  # Create instance: ip_dbg_invert_and_of_0, and set properties
  set ip_dbg_invert_and_of_0 [ create_bd_cell -type ip -vlnv iaea.org:user:ip_dbg_invert_and_offset:1.0 ip_dbg_invert_and_of_0 ]

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins S05_AXI] [get_bd_intf_pins invert_and_offset_0/S00_AXI]
  connect_bd_intf_net -intf_net ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0 [get_bd_intf_pins dbg_invert_and_offset_m0] [get_bd_intf_pins ip_dbg_invert_and_of_0/dbg_invert_and_offset_m0]

  # Create port connections
  connect_bd_net -net clk_1 [get_bd_pins clk_dpp] [get_bd_pins invert_and_offset_0/clk]
  connect_bd_net -net inp_1 [get_bd_pins adc_data] [get_bd_pins invert_and_offset_0/inp] [get_bd_pins ip_dbg_invert_and_of_0/adc_data]
  connect_bd_net -net invert_and_offset_0_outp [get_bd_pins y] [get_bd_pins invert_and_offset_0/outp] [get_bd_pins ip_dbg_invert_and_of_0/outp]
  connect_bd_net -net s00_axi_aclk_1 [get_bd_pins clk_cpu] [get_bd_pins invert_and_offset_0/s00_axi_aclk]
  connect_bd_net -net s00_axi_aresetn_1 [get_bd_pins clk_cpu_aresetn] [get_bd_pins invert_and_offset_0/s00_axi_aresetn]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: pulse_conditioning_slow
proc create_hier_cell_pulse_conditioning_slow { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_pulse_conditioning_slow() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S08_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S09_AXI

  create_bd_intf_pin -mode Master -vlnv iaea.org:interface:dbg_pulse_cond_slow_rtl:1.0 dbg_pulse_cond_slow_m0


  # Create pins
  create_bd_pin -dir I -type clk clk_cpu
  create_bd_pin -dir I -type rst clk_cpu_aresetn
  create_bd_pin -dir I -type clk clk_dpp
  create_bd_pin -dir I -from 15 -to 0 x
  create_bd_pin -dir O -from 15 -to 0 -type data y

  # Create instance: ip_dbg_pulse_cond_sl_0, and set properties
  set ip_dbg_pulse_cond_sl_0 [ create_bd_cell -type ip -vlnv iaea.org:interface:ip_dbg_pulse_cond_slow:1.0 ip_dbg_pulse_cond_sl_0 ]

  # Create instance: ip_dc_stabilizer_0, and set properties
  set ip_dc_stabilizer_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:ip_dc_stabilizer:2.0 ip_dc_stabilizer_0 ]

  # Create instance: ip_shaper_0, and set properties
  set ip_shaper_0 [ create_bd_cell -type ip -vlnv IAEA:IP_Shaper:ip_shaper:4.1 ip_shaper_0 ]

  # Create interface connections
  connect_bd_intf_net -intf_net S08_AXI_1 [get_bd_intf_pins S08_AXI] [get_bd_intf_pins ip_dc_stabilizer_0/AXI_DC_STAB]
  connect_bd_intf_net -intf_net S09_AXI_1 [get_bd_intf_pins S09_AXI] [get_bd_intf_pins ip_shaper_0/axi_clk_domain_s_axi]
  connect_bd_intf_net -intf_net ip_dbg_pulse_cond_sl_0_dbg_pulse_cond_slow_m0 [get_bd_intf_pins dbg_pulse_cond_slow_m0] [get_bd_intf_pins ip_dbg_pulse_cond_sl_0/dbg_pulse_cond_slow_m0]

  # Create port connections
  connect_bd_net -net clk_1 [get_bd_pins clk_dpp] [get_bd_pins ip_dc_stabilizer_0/clk] [get_bd_pins ip_shaper_0/filter_clk_domain_clk]
  connect_bd_net -net clk_out1_1 [get_bd_pins clk_cpu] [get_bd_pins ip_dc_stabilizer_0/axi_dc_stab_aclk] [get_bd_pins ip_shaper_0/axi_clk_domain_clk]
  connect_bd_net -net ip_blr_0_y [get_bd_pins y] [get_bd_pins ip_dbg_pulse_cond_sl_0/blr] [get_bd_pins ip_dbg_pulse_cond_sl_0/dc_stab] [get_bd_pins ip_dbg_pulse_cond_sl_0/dc_stab_acc] [get_bd_pins ip_dc_stabilizer_0/y]
  connect_bd_net -net ip_shaper_0_impulse_out [get_bd_pins ip_dbg_pulse_cond_sl_0/impulse] [get_bd_pins ip_shaper_0/impulse_out]
  connect_bd_net -net ip_shaper_0_rect_out [get_bd_pins ip_dbg_pulse_cond_sl_0/rect] [get_bd_pins ip_shaper_0/rect_out]
  connect_bd_net -net ip_shaper_0_shaper_out [get_bd_pins ip_dbg_pulse_cond_sl_0/shaper] [get_bd_pins ip_dc_stabilizer_0/x] [get_bd_pins ip_shaper_0/shaper_out]
  connect_bd_net -net peripheral_aresetn_1 [get_bd_pins clk_cpu_aresetn] [get_bd_pins ip_dc_stabilizer_0/axi_dc_stab_aresetn] [get_bd_pins ip_dc_stabilizer_0/resetn] [get_bd_pins ip_shaper_0/axi_clk_domain_aresetn]
  connect_bd_net -net x_1 [get_bd_pins x] [get_bd_pins ip_shaper_0/data_in]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: pulse_conditioning_fast
proc create_hier_cell_pulse_conditioning_fast { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_pulse_conditioning_fast() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S14_AXIi

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S15_AXI

  create_bd_intf_pin -mode Master -vlnv iaea.org:interface:dbg_pulse_cond_fast_rtl:1.0 dbg_pulse_cond_fast


  # Create pins
  create_bd_pin -dir I clk_cpu
  create_bd_pin -dir I -type rst clk_cpu_aresetn
  create_bd_pin -dir I clk_dpp
  create_bd_pin -dir I -from 15 -to 0 x
  create_bd_pin -dir O -from 15 -to 0 -type data y

  # Create instance: ip_dbg_pulse_cond_fa_0, and set properties
  set ip_dbg_pulse_cond_fa_0 [ create_bd_cell -type ip -vlnv iaea.org:user:ip_dbg_pulse_cond_fast:1.0 ip_dbg_pulse_cond_fa_0 ]

  # Create instance: ip_dc_stabilizer_0, and set properties
  set ip_dc_stabilizer_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:ip_dc_stabilizer:2.0 ip_dc_stabilizer_0 ]

  # Create instance: ip_shaper_0, and set properties
  set ip_shaper_0 [ create_bd_cell -type ip -vlnv IAEA:IP_Shaper:ip_shaper:4.1 ip_shaper_0 ]

  # Create interface connections
  connect_bd_intf_net -intf_net S14_AXIi_1 [get_bd_intf_pins S14_AXIi] [get_bd_intf_pins ip_shaper_0/axi_clk_domain_s_axi]
  connect_bd_intf_net -intf_net S16_AXI_1 [get_bd_intf_pins S15_AXI] [get_bd_intf_pins ip_dc_stabilizer_0/AXI_DC_STAB]
  connect_bd_intf_net -intf_net ip_dbg_pulse_cond_fa_0_dbg_pulse_cond_fast [get_bd_intf_pins dbg_pulse_cond_fast] [get_bd_intf_pins ip_dbg_pulse_cond_fa_0/dbg_pulse_cond_fast]

  # Create port connections
  connect_bd_net -net clk_cpu_1 [get_bd_pins clk_cpu] [get_bd_pins ip_dc_stabilizer_0/axi_dc_stab_aclk] [get_bd_pins ip_shaper_0/axi_clk_domain_clk]
  connect_bd_net -net clk_dpp_1 [get_bd_pins clk_dpp] [get_bd_pins ip_dc_stabilizer_0/clk] [get_bd_pins ip_shaper_0/filter_clk_domain_clk]
  connect_bd_net -net ip_blr_0_y [get_bd_pins y] [get_bd_pins ip_dbg_pulse_cond_fa_0/blr] [get_bd_pins ip_dbg_pulse_cond_fa_0/dc_stab] [get_bd_pins ip_dbg_pulse_cond_fa_0/dc_stab_acc] [get_bd_pins ip_dc_stabilizer_0/y]
  connect_bd_net -net ip_shaper_0_impulse_out [get_bd_pins ip_dbg_pulse_cond_fa_0/impulse] [get_bd_pins ip_shaper_0/impulse_out]
  connect_bd_net -net ip_shaper_0_rect_out [get_bd_pins ip_dbg_pulse_cond_fa_0/rect] [get_bd_pins ip_shaper_0/rect_out]
  connect_bd_net -net ip_shaper_0_shaper_out [get_bd_pins ip_dbg_pulse_cond_fa_0/shaper] [get_bd_pins ip_dc_stabilizer_0/x] [get_bd_pins ip_shaper_0/shaper_out]
  connect_bd_net -net peripheral_aresetn_1 [get_bd_pins clk_cpu_aresetn] [get_bd_pins ip_dc_stabilizer_0/axi_dc_stab_aresetn] [get_bd_pins ip_dc_stabilizer_0/resetn] [get_bd_pins ip_shaper_0/axi_clk_domain_aresetn]
  connect_bd_net -net x_1 [get_bd_pins x] [get_bd_pins ip_shaper_0/data_in]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: pha
proc create_hier_cell_pha { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_pha() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S07_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S10_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S11_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S12_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S16_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S17_AXI

  create_bd_intf_pin -mode Master -vlnv iaea.org:interface:dbg_pha_rtl:1.0 dbg_pha_m0


  # Create pins
  create_bd_pin -dir I -type clk clk_cpu
  create_bd_pin -dir I -type rst clk_cpu_aresetn
  create_bd_pin -dir I clk_dpp
  create_bd_pin -dir I -from 15 -to 0 -type data x_f
  create_bd_pin -dir I -from 15 -to 0 -type data x_s

  # Create instance: glue_logic
  create_hier_cell_glue_logic $hier_obj glue_logic

  # Create instance: ip_dbg_pha_0, and set properties
  set ip_dbg_pha_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:ip_dbg_pha:1.0 ip_dbg_pha_0 ]

  # Create instance: ip_timers_0, and set properties
  set ip_timers_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:ip_timers:3.0 ip_timers_0 ]

  # Create instance: mca
  create_hier_cell_mca $hier_obj mca

  # Create instance: pkd
  create_hier_cell_pkd $hier_obj pkd

  # Create instance: pur
  create_hier_cell_pur $hier_obj pur

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins S07_AXI] [get_bd_intf_pins ip_timers_0/S00_AXI]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins S10_AXI] [get_bd_intf_pins mca/S10_AXI]
  connect_bd_intf_net -intf_net Conn3 [get_bd_intf_pins S11_AXI] [get_bd_intf_pins pkd/S11_AXI]
  connect_bd_intf_net -intf_net Conn4 [get_bd_intf_pins S12_AXI] [get_bd_intf_pins pkd/S12_AXI]
  connect_bd_intf_net -intf_net Conn5 [get_bd_intf_pins S16_AXI] [get_bd_intf_pins pur/S16_AXI]
  connect_bd_intf_net -intf_net Conn6 [get_bd_intf_pins S17_AXI] [get_bd_intf_pins pur/S17_AXI]
  connect_bd_intf_net -intf_net ip_dbg_pha_0_dbg_pha_m0 [get_bd_intf_pins dbg_pha_m0] [get_bd_intf_pins ip_dbg_pha_0/dbg_pha_m0]

  # Create port connections
  connect_bd_net -net D_1 [get_bd_pins x_s] [get_bd_pins pkd/x]
  connect_bd_net -net In2_1 [get_bd_pins glue_logic/In2] [get_bd_pins pur/rejectn]
  connect_bd_net -net clk_1 [get_bd_pins clk_dpp] [get_bd_pins ip_timers_0/clk] [get_bd_pins mca/clk_dpp] [get_bd_pins pkd/clk_dpp] [get_bd_pins pur/clk_dpp]
  connect_bd_net -net clk_out1_1 [get_bd_pins clk_cpu] [get_bd_pins ip_timers_0/s00_axi_aclk] [get_bd_pins mca/clk_cpu] [get_bd_pins pkd/clk_cpu] [get_bd_pins pur/clk_cpu]
  connect_bd_net -net glue_logic_Res [get_bd_pins glue_logic/Res] [get_bd_pins mca/peak_amp_rdy]
  connect_bd_net -net glue_logic_Res1 [get_bd_pins glue_logic/Res1] [get_bd_pins ip_dbg_pha_0/rejectn]
  connect_bd_net -net ip_timers_0_bram_seg [get_bd_pins ip_timers_0/bram_seg] [get_bd_pins mca/segment]
  connect_bd_net -net ip_timers_0_timers_enabled [get_bd_pins glue_logic/In0] [get_bd_pins ip_timers_0/timers_enabled]
  connect_bd_net -net peripheral_aresetn_1 [get_bd_pins clk_cpu_aresetn] [get_bd_pins ip_timers_0/s00_axi_aresetn] [get_bd_pins mca/clk_cpu_aresetn] [get_bd_pins pkd/clk_cpu_aresetn] [get_bd_pins pur/clk_cpu_aresetn]
  connect_bd_net -net pkd_peak_amp [get_bd_pins ip_dbg_pha_0/peak_det_signal] [get_bd_pins mca/peak_amp] [get_bd_pins pkd/peak_amp]
  connect_bd_net -net pkd_peak_amp_rdy [get_bd_pins glue_logic/In1] [get_bd_pins ip_dbg_pha_0/peak_amp_rdy_slow] [get_bd_pins pkd/peak_amp_rdy]
  connect_bd_net -net pur_lt_dir [get_bd_pins ip_timers_0/lt_dir] [get_bd_pins pur/lt_dir]
  connect_bd_net -net pur_peak_amp_rdy [get_bd_pins ip_dbg_pha_0/peak_amp_rdy_fast] [get_bd_pins pur/peak_amp_rdy]
  connect_bd_net -net x_1 [get_bd_pins x_f] [get_bd_pins pur/x]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: ps_mb_0
proc create_hier_cell_ps_mb_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_ps_mb_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M00_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M01_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M02_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M03_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M05_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M06_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M07_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M08_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M09_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M10_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M11_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M12_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M13_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M14_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M15_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M16_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M17_AXI


  # Create pins
  create_bd_pin -dir I -type clk Clk
  create_bd_pin -dir I dcm_locked
  create_bd_pin -dir I -from 0 -to 0 -type intr intr
  create_bd_pin -dir O -from 0 -to 0 -type rst peripheral_aresetn

  # Create instance: axi_intc_0, and set properties
  set axi_intc_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_intc:4.1 axi_intc_0 ]

  # Create instance: mdm_1, and set properties
  set mdm_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:mdm:3.2 mdm_1 ]

  # Create instance: microblaze_0, and set properties
  set microblaze_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:microblaze:11.0 microblaze_0 ]
  set_property -dict [ list \
   CONFIG.C_DEBUG_ENABLED {1} \
   CONFIG.C_D_AXI {1} \
   CONFIG.C_D_LMB {1} \
   CONFIG.C_I_LMB {1} \
 ] $microblaze_0

  # Create instance: microblaze_0_axi_periph, and set properties
  set microblaze_0_axi_periph [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 microblaze_0_axi_periph ]
  set_property -dict [ list \
   CONFIG.NUM_MI {18} \
 ] $microblaze_0_axi_periph

  # Create instance: microblaze_0_local_memory
  create_hier_cell_microblaze_0_local_memory $hier_obj microblaze_0_local_memory

  # Create instance: rst_clk_wiz_0_100M, and set properties
  set rst_clk_wiz_0_100M [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 rst_clk_wiz_0_100M ]

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins M01_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M01_AXI]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins M00_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M00_AXI]
  connect_bd_intf_net -intf_net Conn3 [get_bd_intf_pins M02_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M02_AXI]
  connect_bd_intf_net -intf_net Conn4 [get_bd_intf_pins M05_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M05_AXI]
  connect_bd_intf_net -intf_net Conn5 [get_bd_intf_pins M03_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M03_AXI]
  connect_bd_intf_net -intf_net Conn6 [get_bd_intf_pins M06_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M06_AXI]
  connect_bd_intf_net -intf_net axi_intc_0_interrupt [get_bd_intf_pins axi_intc_0/interrupt] [get_bd_intf_pins microblaze_0/INTERRUPT]
  connect_bd_intf_net -intf_net mdm_1_MBDEBUG_0 [get_bd_intf_pins mdm_1/MBDEBUG_0] [get_bd_intf_pins microblaze_0/DEBUG]
  connect_bd_intf_net -intf_net microblaze_0_DLMB [get_bd_intf_pins microblaze_0/DLMB] [get_bd_intf_pins microblaze_0_local_memory/DLMB]
  connect_bd_intf_net -intf_net microblaze_0_ILMB [get_bd_intf_pins microblaze_0/ILMB] [get_bd_intf_pins microblaze_0_local_memory/ILMB]
  connect_bd_intf_net -intf_net microblaze_0_M_AXI_DP [get_bd_intf_pins microblaze_0/M_AXI_DP] [get_bd_intf_pins microblaze_0_axi_periph/S00_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M04_AXI [get_bd_intf_pins axi_intc_0/s_axi] [get_bd_intf_pins microblaze_0_axi_periph/M04_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M07_AXI [get_bd_intf_pins M07_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M07_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M08_AXI [get_bd_intf_pins M08_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M08_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M09_AXI [get_bd_intf_pins M09_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M09_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M10_AXI [get_bd_intf_pins M10_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M10_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M11_AXI [get_bd_intf_pins M11_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M11_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M12_AXI [get_bd_intf_pins M12_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M12_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M13_AXI [get_bd_intf_pins M13_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M13_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M14_AXI [get_bd_intf_pins M14_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M14_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M15_AXI [get_bd_intf_pins M15_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M15_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M16_AXI [get_bd_intf_pins M16_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M16_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M17_AXI [get_bd_intf_pins M17_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M17_AXI]

  # Create port connections
  connect_bd_net -net Clk_1 [get_bd_pins Clk] [get_bd_pins axi_intc_0/s_axi_aclk] [get_bd_pins microblaze_0/Clk] [get_bd_pins microblaze_0_axi_periph/ACLK] [get_bd_pins microblaze_0_axi_periph/M00_ACLK] [get_bd_pins microblaze_0_axi_periph/M01_ACLK] [get_bd_pins microblaze_0_axi_periph/M02_ACLK] [get_bd_pins microblaze_0_axi_periph/M03_ACLK] [get_bd_pins microblaze_0_axi_periph/M04_ACLK] [get_bd_pins microblaze_0_axi_periph/M05_ACLK] [get_bd_pins microblaze_0_axi_periph/M06_ACLK] [get_bd_pins microblaze_0_axi_periph/M07_ACLK] [get_bd_pins microblaze_0_axi_periph/M08_ACLK] [get_bd_pins microblaze_0_axi_periph/M09_ACLK] [get_bd_pins microblaze_0_axi_periph/M10_ACLK] [get_bd_pins microblaze_0_axi_periph/M11_ACLK] [get_bd_pins microblaze_0_axi_periph/M12_ACLK] [get_bd_pins microblaze_0_axi_periph/M13_ACLK] [get_bd_pins microblaze_0_axi_periph/M14_ACLK] [get_bd_pins microblaze_0_axi_periph/M15_ACLK] [get_bd_pins microblaze_0_axi_periph/M16_ACLK] [get_bd_pins microblaze_0_axi_periph/M17_ACLK] [get_bd_pins microblaze_0_axi_periph/S00_ACLK] [get_bd_pins microblaze_0_local_memory/LMB_Clk] [get_bd_pins rst_clk_wiz_0_100M/slowest_sync_clk]
  connect_bd_net -net dcm_locked_1 [get_bd_pins dcm_locked] [get_bd_pins rst_clk_wiz_0_100M/dcm_locked]
  connect_bd_net -net intr_1 [get_bd_pins intr] [get_bd_pins axi_intc_0/intr]
  connect_bd_net -net mdm_1_Debug_SYS_Rst [get_bd_pins mdm_1/Debug_SYS_Rst] [get_bd_pins rst_clk_wiz_0_100M/mb_debug_sys_rst]
  connect_bd_net -net rst_clk_wiz_0_100M_bus_struct_reset [get_bd_pins microblaze_0_local_memory/SYS_Rst] [get_bd_pins rst_clk_wiz_0_100M/bus_struct_reset]
  connect_bd_net -net rst_clk_wiz_0_100M_mb_reset [get_bd_pins microblaze_0/Reset] [get_bd_pins rst_clk_wiz_0_100M/mb_reset]
  connect_bd_net -net rst_clk_wiz_0_100M_peripheral_aresetn [get_bd_pins peripheral_aresetn] [get_bd_pins axi_intc_0/s_axi_aresetn] [get_bd_pins microblaze_0_axi_periph/ARESETN] [get_bd_pins microblaze_0_axi_periph/M00_ARESETN] [get_bd_pins microblaze_0_axi_periph/M01_ARESETN] [get_bd_pins microblaze_0_axi_periph/M02_ARESETN] [get_bd_pins microblaze_0_axi_periph/M03_ARESETN] [get_bd_pins microblaze_0_axi_periph/M04_ARESETN] [get_bd_pins microblaze_0_axi_periph/M05_ARESETN] [get_bd_pins microblaze_0_axi_periph/M06_ARESETN] [get_bd_pins microblaze_0_axi_periph/M07_ARESETN] [get_bd_pins microblaze_0_axi_periph/M08_ARESETN] [get_bd_pins microblaze_0_axi_periph/M09_ARESETN] [get_bd_pins microblaze_0_axi_periph/M10_ARESETN] [get_bd_pins microblaze_0_axi_periph/M11_ARESETN] [get_bd_pins microblaze_0_axi_periph/M12_ARESETN] [get_bd_pins microblaze_0_axi_periph/M13_ARESETN] [get_bd_pins microblaze_0_axi_periph/M14_ARESETN] [get_bd_pins microblaze_0_axi_periph/M15_ARESETN] [get_bd_pins microblaze_0_axi_periph/M16_ARESETN] [get_bd_pins microblaze_0_axi_periph/M17_ARESETN] [get_bd_pins microblaze_0_axi_periph/S00_ARESETN] [get_bd_pins rst_clk_wiz_0_100M/peripheral_aresetn]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins rst_clk_wiz_0_100M/ext_reset_in] [get_bd_pins xlconstant_0/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: mb_periph_0
proc create_hier_cell_mb_periph_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_mb_periph_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S00_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S01_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S02_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S06_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:spi_rtl:1.0 ads4149_spi

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:gpio_rtl:1.0 gpio_1

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:gpio_rtl:1.0 gpio_2

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:uart_rtl:1.0 rs232_uart


  # Create pins
  create_bd_pin -dir I -type clk clk_cpu
  create_bd_pin -dir I -type rst clk_cpu_aresetn

  # Create instance: axi_gpio_0, and set properties
  set axi_gpio_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_0 ]
  set_property -dict [ list \
   CONFIG.GPIO_BOARD_INTERFACE {gpio_1} \
   CONFIG.USE_BOARD_FLOW {true} \
 ] $axi_gpio_0

  # Create instance: axi_gpio_1, and set properties
  set axi_gpio_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_1 ]
  set_property -dict [ list \
   CONFIG.GPIO_BOARD_INTERFACE {gpio_2} \
   CONFIG.USE_BOARD_FLOW {true} \
 ] $axi_gpio_1

  # Create instance: axi_quad_spi_0, and set properties
  set axi_quad_spi_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_quad_spi:3.2 axi_quad_spi_0 ]
  set_property -dict [ list \
   CONFIG.QSPI_BOARD_INTERFACE {ads4149_spi} \
   CONFIG.USE_BOARD_FLOW {true} \
 ] $axi_quad_spi_0

  # Create instance: axi_uartlite_0, and set properties
  set axi_uartlite_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_uartlite:2.0 axi_uartlite_0 ]
  set_property -dict [ list \
   CONFIG.C_BAUDRATE {115200} \
   CONFIG.UARTLITE_BOARD_INTERFACE {rs232_uart} \
   CONFIG.USE_BOARD_FLOW {true} \
 ] $axi_uartlite_0

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins S01_AXI] [get_bd_intf_pins axi_gpio_0/S_AXI]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins gpio_1] [get_bd_intf_pins axi_gpio_0/GPIO]
  connect_bd_intf_net -intf_net Conn3 [get_bd_intf_pins ads4149_spi] [get_bd_intf_pins axi_quad_spi_0/SPI_0]
  connect_bd_intf_net -intf_net Conn4 [get_bd_intf_pins S00_AXI] [get_bd_intf_pins axi_quad_spi_0/AXI_LITE]
  connect_bd_intf_net -intf_net Conn5 [get_bd_intf_pins rs232_uart] [get_bd_intf_pins axi_uartlite_0/UART]
  connect_bd_intf_net -intf_net Conn7 [get_bd_intf_pins S06_AXI] [get_bd_intf_pins axi_gpio_1/S_AXI]
  connect_bd_intf_net -intf_net Conn8 [get_bd_intf_pins gpio_2] [get_bd_intf_pins axi_gpio_1/GPIO]
  connect_bd_intf_net -intf_net S02_AXI_1 [get_bd_intf_pins S02_AXI] [get_bd_intf_pins axi_uartlite_0/S_AXI]

  # Create port connections
  connect_bd_net -net s_axi_aclk_1 [get_bd_pins clk_cpu] [get_bd_pins axi_gpio_0/s_axi_aclk] [get_bd_pins axi_gpio_1/s_axi_aclk] [get_bd_pins axi_quad_spi_0/ext_spi_clk] [get_bd_pins axi_quad_spi_0/s_axi_aclk] [get_bd_pins axi_uartlite_0/s_axi_aclk]
  connect_bd_net -net s_axi_aresetn_1 [get_bd_pins clk_cpu_aresetn] [get_bd_pins axi_gpio_0/s_axi_aresetn] [get_bd_pins axi_gpio_1/s_axi_aresetn] [get_bd_pins axi_quad_spi_0/s_axi_aresetn] [get_bd_pins axi_uartlite_0/s_axi_aresetn]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: dpp_0
proc create_hier_cell_dpp_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_dpp_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S03_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S05_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S07_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S08_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S09_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S10_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S11_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S12_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S13_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S14_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S15_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S16_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S17_AXI


  # Create pins
  create_bd_pin -dir I -from 13 -to 0 adc_data
  create_bd_pin -dir I -type clk clk_cpu
  create_bd_pin -dir I -type rst clk_cpu_aresetn
  create_bd_pin -dir I clk_dpp
  create_bd_pin -dir O -from 0 -to 0 -type intr full

  # Create instance: pha
  create_hier_cell_pha $hier_obj pha

  # Create instance: pulse_conditioning_fast
  create_hier_cell_pulse_conditioning_fast $hier_obj pulse_conditioning_fast

  # Create instance: pulse_conditioning_slow
  create_hier_cell_pulse_conditioning_slow $hier_obj pulse_conditioning_slow

  # Create instance: pulse_offseting
  create_hier_cell_pulse_offseting $hier_obj pulse_offseting

  # Create instance: scope
  create_hier_cell_scope $hier_obj scope

  # Create interface connections
  connect_bd_intf_net -intf_net AXI_DC_STAB_1 [get_bd_intf_pins S08_AXI] [get_bd_intf_pins pulse_conditioning_slow/S08_AXI]
  connect_bd_intf_net -intf_net AXI_DC_STAB_2 [get_bd_intf_pins S15_AXI] [get_bd_intf_pins pulse_conditioning_fast/S15_AXI]
  connect_bd_intf_net -intf_net S00_AXI1_1 [get_bd_intf_pins S07_AXI] [get_bd_intf_pins pha/S07_AXI]
  connect_bd_intf_net -intf_net S00_AXI3_1 [get_bd_intf_pins S11_AXI] [get_bd_intf_pins pha/S11_AXI]
  connect_bd_intf_net -intf_net S00_AXI_1 [get_bd_intf_pins S05_AXI] [get_bd_intf_pins pulse_offseting/S05_AXI]
  connect_bd_intf_net -intf_net S00_AXI_2 [get_bd_intf_pins S13_AXI] [get_bd_intf_pins scope/S13_AXI]
  connect_bd_intf_net -intf_net S00_AXI_3 [get_bd_intf_pins S17_AXI] [get_bd_intf_pins pha/S17_AXI]
  connect_bd_intf_net -intf_net S_AXI_1 [get_bd_intf_pins S10_AXI] [get_bd_intf_pins pha/S10_AXI]
  connect_bd_intf_net -intf_net axi_clk_domain_s_axi_1 [get_bd_intf_pins S09_AXI] [get_bd_intf_pins pulse_conditioning_slow/S09_AXI]
  connect_bd_intf_net -intf_net axi_clk_domain_s_axi_2 [get_bd_intf_pins S14_AXI] [get_bd_intf_pins pulse_conditioning_fast/S14_AXIi]
  connect_bd_intf_net -intf_net axibusdomain_s_axi1_1 [get_bd_intf_pins S12_AXI] [get_bd_intf_pins pha/S12_AXI]
  connect_bd_intf_net -intf_net axibusdomain_s_axi_1 [get_bd_intf_pins S03_AXI] [get_bd_intf_pins scope/S03_AXI]
  connect_bd_intf_net -intf_net axibusdomain_s_axi_2 [get_bd_intf_pins S16_AXI] [get_bd_intf_pins pha/S16_AXI]
  connect_bd_intf_net -intf_net pha_dbg_pha_m0 [get_bd_intf_pins pha/dbg_pha_m0] [get_bd_intf_pins scope/dbg_pha_s0]
  connect_bd_intf_net -intf_net pulse_conditioning_fast_dbg_pulse_cond_fast [get_bd_intf_pins pulse_conditioning_fast/dbg_pulse_cond_fast] [get_bd_intf_pins scope/dbg_pulse_cond_fast_s0]
  connect_bd_intf_net -intf_net pulse_conditioning_slow_dbg_pulse_cond_slow_m0 [get_bd_intf_pins pulse_conditioning_slow/dbg_pulse_cond_slow_m0] [get_bd_intf_pins scope/dbg_pulse_cond_slow_s0]
  connect_bd_intf_net -intf_net pulse_offseting_dbg_invert_and_offset_m0 [get_bd_intf_pins pulse_offseting/dbg_invert_and_offset_m0] [get_bd_intf_pins scope/dbg_invert_and_offset_s0]

  # Create port connections
  connect_bd_net -net ch2_1 [get_bd_pins pulse_conditioning_fast/x] [get_bd_pins pulse_conditioning_slow/x] [get_bd_pins pulse_offseting/y]
  connect_bd_net -net clk_1 [get_bd_pins clk_dpp] [get_bd_pins pha/clk_dpp] [get_bd_pins pulse_conditioning_fast/clk_dpp] [get_bd_pins pulse_conditioning_slow/clk_dpp] [get_bd_pins pulse_offseting/clk_dpp] [get_bd_pins scope/clk_dpp]
  connect_bd_net -net inp_1 [get_bd_pins adc_data] [get_bd_pins pulse_offseting/adc_data]
  connect_bd_net -net pulse_conditioning_slow_y [get_bd_pins pha/x_s] [get_bd_pins pulse_conditioning_slow/y]
  connect_bd_net -net s00_axi_aclk_1 [get_bd_pins clk_cpu] [get_bd_pins pha/clk_cpu] [get_bd_pins pulse_conditioning_fast/clk_cpu] [get_bd_pins pulse_conditioning_slow/clk_cpu] [get_bd_pins pulse_offseting/clk_cpu] [get_bd_pins scope/clk_cpu]
  connect_bd_net -net s00_axi_aresetn_1 [get_bd_pins clk_cpu_aresetn] [get_bd_pins pha/clk_cpu_aresetn] [get_bd_pins pulse_conditioning_fast/clk_cpu_aresetn] [get_bd_pins pulse_conditioning_slow/clk_cpu_aresetn] [get_bd_pins pulse_offseting/clk_cpu_aresetn] [get_bd_pins scope/clk_cpu_aresetn]
  connect_bd_net -net scope_full [get_bd_pins full] [get_bd_pins scope/full]
  connect_bd_net -net x_1 [get_bd_pins pha/x_f] [get_bd_pins pulse_conditioning_fast/y]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: adc_data_0
proc create_hier_cell_adc_data_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_adc_data_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv iaea.org:interface:ddr_rtl:1.0 ads4149_in


  # Create pins
  create_bd_pin -dir O -from 13 -to 0 adc_data
  create_bd_pin -dir I clk_dpp
  create_bd_pin -dir I clk_ref

  # Create instance: CDCC_0, and set properties
  set CDCC_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:CDCC:1.0 CDCC_0 ]

  # Create instance: ddr2sdr_0, and set properties
  set ddr2sdr_0 [ create_bd_cell -type ip -vlnv iaea.org:ip:ddr2sdr:1.0 ddr2sdr_0 ]
  set_property -dict [ list \
   CONFIG.DDR_BOARD_INTERFACE {ads4149_in} \
 ] $ddr2sdr_0

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins ads4149_in] [get_bd_intf_pins ddr2sdr_0/DDR]

  # Create port connections
  connect_bd_net -net CDCC_0_data_out [get_bd_pins adc_data] [get_bd_pins CDCC_0/data_out]
  connect_bd_net -net clk_dst_1 [get_bd_pins clk_dpp] [get_bd_pins CDCC_0/clk_dst]
  connect_bd_net -net ddr2sdr_0_adc_clk [get_bd_pins CDCC_0/clk_src] [get_bd_pins ddr2sdr_0/adc_clk]
  connect_bd_net -net ddr2sdr_0_adc_data [get_bd_pins CDCC_0/data_in] [get_bd_pins ddr2sdr_0/adc_data]
  connect_bd_net -net refclk_1 [get_bd_pins clk_ref] [get_bd_pins ddr2sdr_0/refclk]

  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set ads4149_in [ create_bd_intf_port -mode Slave -vlnv iaea.org:interface:ddr_rtl:1.0 ads4149_in ]

  set ads4149_spi [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:spi_rtl:1.0 ads4149_spi ]

  set gpio_1 [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:gpio_rtl:1.0 gpio_1 ]

  set gpio_2 [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:gpio_rtl:1.0 gpio_2 ]

  set rs232_uart [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:uart_rtl:1.0 rs232_uart ]

  set sys_diff_clock [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 sys_diff_clock ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {250000000} \
   ] $sys_diff_clock


  # Create ports

  # Create instance: adc_data_0
  create_hier_cell_adc_data_0 [current_bd_instance .] adc_data_0

  # Create instance: clk_wiz_0, and set properties
  set clk_wiz_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:6.0 clk_wiz_0 ]
  set_property -dict [ list \
   CONFIG.CLKOUT2_JITTER {123.073} \
   CONFIG.CLKOUT2_PHASE_ERROR {85.928} \
   CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {50.000} \
   CONFIG.CLKOUT2_USED {true} \
   CONFIG.CLKOUT3_JITTER {93.521} \
   CONFIG.CLKOUT3_PHASE_ERROR {85.928} \
   CONFIG.CLKOUT3_REQUESTED_OUT_FREQ {200.000} \
   CONFIG.CLKOUT3_USED {true} \
   CONFIG.CLK_IN1_BOARD_INTERFACE {sys_diff_clock} \
   CONFIG.CLK_OUT1_PORT {clk_cpu} \
   CONFIG.CLK_OUT2_PORT {clk_dpp} \
   CONFIG.CLK_OUT3_PORT {clk_ref} \
   CONFIG.MMCM_CLKOUT1_DIVIDE {20} \
   CONFIG.MMCM_CLKOUT2_DIVIDE {5} \
   CONFIG.NUM_OUT_CLKS {3} \
   CONFIG.USE_BOARD_FLOW {true} \
   CONFIG.USE_RESET {false} \
 ] $clk_wiz_0

  # Create instance: dpp_0
  create_hier_cell_dpp_0 [current_bd_instance .] dpp_0

  # Create instance: mb_periph_0
  create_hier_cell_mb_periph_0 [current_bd_instance .] mb_periph_0

  # Create instance: ps_mb_0
  create_hier_cell_ps_mb_0 [current_bd_instance .] ps_mb_0

  # Create instance: system_ila_0, and set properties
  set system_ila_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:system_ila:1.1 system_ila_0 ]
  set_property -dict [ list \
   CONFIG.C_BRAM_CNT {0.5} \
   CONFIG.C_DATA_DEPTH {4096} \
   CONFIG.C_MON_TYPE {NATIVE} \
 ] $system_ila_0

  # Create interface connections
  connect_bd_intf_net -intf_net S00_AXI_1 [get_bd_intf_pins dpp_0/S05_AXI] [get_bd_intf_pins ps_mb_0/M05_AXI]
  connect_bd_intf_net -intf_net ads4149_in_1 [get_bd_intf_ports ads4149_in] [get_bd_intf_pins adc_data_0/ads4149_in]
  connect_bd_intf_net -intf_net axibusdomain_s_axi_1 [get_bd_intf_pins dpp_0/S03_AXI] [get_bd_intf_pins ps_mb_0/M03_AXI]
  connect_bd_intf_net -intf_net mb_periph_0_ads4149_spi [get_bd_intf_ports ads4149_spi] [get_bd_intf_pins mb_periph_0/ads4149_spi]
  connect_bd_intf_net -intf_net mb_periph_0_gpio_1 [get_bd_intf_ports gpio_1] [get_bd_intf_pins mb_periph_0/gpio_1]
  connect_bd_intf_net -intf_net mb_periph_0_gpio_2 [get_bd_intf_ports gpio_2] [get_bd_intf_pins mb_periph_0/gpio_2]
  connect_bd_intf_net -intf_net mb_periph_0_rs232_uart [get_bd_intf_ports rs232_uart] [get_bd_intf_pins mb_periph_0/rs232_uart]
  connect_bd_intf_net -intf_net ps_mb_0_M00_AXI [get_bd_intf_pins mb_periph_0/S00_AXI] [get_bd_intf_pins ps_mb_0/M00_AXI]
  connect_bd_intf_net -intf_net ps_mb_0_M01_AXI [get_bd_intf_pins mb_periph_0/S01_AXI] [get_bd_intf_pins ps_mb_0/M01_AXI]
  connect_bd_intf_net -intf_net ps_mb_0_M02_AXI [get_bd_intf_pins mb_periph_0/S02_AXI] [get_bd_intf_pins ps_mb_0/M02_AXI]
  connect_bd_intf_net -intf_net ps_mb_0_M06_AXI [get_bd_intf_pins mb_periph_0/S06_AXI] [get_bd_intf_pins ps_mb_0/M06_AXI]
  connect_bd_intf_net -intf_net ps_mb_0_M07_AXI [get_bd_intf_pins dpp_0/S07_AXI] [get_bd_intf_pins ps_mb_0/M07_AXI]
  connect_bd_intf_net -intf_net ps_mb_0_M08_AXI [get_bd_intf_pins dpp_0/S08_AXI] [get_bd_intf_pins ps_mb_0/M08_AXI]
  connect_bd_intf_net -intf_net ps_mb_0_M10_AXI [get_bd_intf_pins dpp_0/S09_AXI] [get_bd_intf_pins ps_mb_0/M09_AXI]
  connect_bd_intf_net -intf_net ps_mb_0_M11_AXI [get_bd_intf_pins dpp_0/S10_AXI] [get_bd_intf_pins ps_mb_0/M10_AXI]
  connect_bd_intf_net -intf_net ps_mb_0_M12_AXI [get_bd_intf_pins dpp_0/S11_AXI] [get_bd_intf_pins ps_mb_0/M11_AXI]
  connect_bd_intf_net -intf_net ps_mb_0_M13_AXI [get_bd_intf_pins dpp_0/S12_AXI] [get_bd_intf_pins ps_mb_0/M12_AXI]
  connect_bd_intf_net -intf_net ps_mb_0_M14_AXI [get_bd_intf_pins dpp_0/S13_AXI] [get_bd_intf_pins ps_mb_0/M13_AXI]
  connect_bd_intf_net -intf_net ps_mb_0_M15_AXI [get_bd_intf_pins dpp_0/S14_AXI] [get_bd_intf_pins ps_mb_0/M14_AXI]
  connect_bd_intf_net -intf_net ps_mb_0_M16_AXI [get_bd_intf_pins dpp_0/S15_AXI] [get_bd_intf_pins ps_mb_0/M15_AXI]
  connect_bd_intf_net -intf_net ps_mb_0_M18_AXI [get_bd_intf_pins dpp_0/S16_AXI] [get_bd_intf_pins ps_mb_0/M16_AXI]
  connect_bd_intf_net -intf_net ps_mb_0_M19_AXI [get_bd_intf_pins dpp_0/S17_AXI] [get_bd_intf_pins ps_mb_0/M17_AXI]
  connect_bd_intf_net -intf_net sys_diff_clock_1 [get_bd_intf_ports sys_diff_clock] [get_bd_intf_pins clk_wiz_0/CLK_IN1_D]

  # Create port connections
  connect_bd_net -net CDCC_0_data_out [get_bd_pins adc_data_0/adc_data] [get_bd_pins dpp_0/adc_data] [get_bd_pins system_ila_0/probe0]
  connect_bd_net -net clk_wiz_0_clk_dpp [get_bd_pins adc_data_0/clk_dpp] [get_bd_pins clk_wiz_0/clk_dpp] [get_bd_pins dpp_0/clk_dpp] [get_bd_pins system_ila_0/clk]
  connect_bd_net -net clk_wiz_0_clk_ref [get_bd_pins adc_data_0/clk_ref] [get_bd_pins clk_wiz_0/clk_ref]
  connect_bd_net -net clk_wiz_0_locked [get_bd_pins clk_wiz_0/locked] [get_bd_pins ps_mb_0/dcm_locked]
  connect_bd_net -net dpp_full [get_bd_pins dpp_0/full] [get_bd_pins ps_mb_0/intr]
  connect_bd_net -net microblaze_0_Clk [get_bd_pins clk_wiz_0/clk_cpu] [get_bd_pins dpp_0/clk_cpu] [get_bd_pins mb_periph_0/clk_cpu] [get_bd_pins ps_mb_0/Clk]
  connect_bd_net -net rst_clk_wiz_0_100M_peripheral_aresetn [get_bd_pins dpp_0/clk_cpu_aresetn] [get_bd_pins mb_periph_0/clk_cpu_aresetn] [get_bd_pins ps_mb_0/peripheral_aresetn]

  # Create address segments
  create_bd_addr_seg -range 0x00010000 -offset 0x44A70000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs dpp_0/pha/pkd/ShiftRegisterControl_0/S00_AXI/S00_AXI_reg] SEG_ShiftRegisterControl_0_S00_AXI_reg
  create_bd_addr_seg -range 0x00002000 -offset 0xC0000000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs dpp_0/pha/mca/axi_bram_ctrl_0/S_AXI/Mem0] SEG_axi_bram_ctrl_0_Mem0
  create_bd_addr_seg -range 0x00010000 -offset 0x40000000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs mb_periph_0/axi_gpio_0/S_AXI/Reg] SEG_axi_gpio_0_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x40010000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs mb_periph_0/axi_gpio_1/S_AXI/Reg] SEG_axi_gpio_1_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x41200000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs ps_mb_0/axi_intc_0/S_AXI/Reg] SEG_axi_intc_0_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x44A00000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs mb_periph_0/axi_quad_spi_0/AXI_LITE/Reg] SEG_axi_quad_spi_0_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x40600000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs mb_periph_0/axi_uartlite_0/S_AXI/Reg] SEG_axi_uartlite_0_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x00000000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs ps_mb_0/microblaze_0_local_memory/dlmb_bram_if_cntlr/SLMB/Mem] SEG_dlmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x00010000 -offset 0x00000000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Instruction] [get_bd_addr_segs ps_mb_0/microblaze_0_local_memory/ilmb_bram_if_cntlr/SLMB/Mem] SEG_ilmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x00010000 -offset 0x44A20000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs dpp_0/pulse_offseting/invert_and_offset_0/S00_AXI/S00_AXI_reg] SEG_invert_and_offset_0_S00_AXI_reg
  create_bd_addr_seg -range 0x00010000 -offset 0x44AB0000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs dpp_0/pulse_conditioning_fast/ip_dc_stabilizer_0/AXI_DC_STAB/AXI_DC_STAB_reg] SEG_ip_dc_stabilizer_0_AXI_DC_STAB_reg
  create_bd_addr_seg -range 0x00010000 -offset 0x44AC0000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs dpp_0/pulse_conditioning_slow/ip_dc_stabilizer_0/AXI_DC_STAB/AXI_DC_STAB_reg] SEG_ip_dc_stabilizer_0_AXI_DC_STAB_reg1
  create_bd_addr_seg -range 0x00010000 -offset 0x44A90000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs dpp_0/scope/ip_mux16_2_if_0/S00_AXI/S00_AXI_reg] SEG_ip_mux16_2_if_0_S00_AXI_reg
  create_bd_addr_seg -range 0x00010000 -offset 0x44A80000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs dpp_0/pha/pkd/ip_peakdetector_0/axibusdomain_s_axi/reg0] SEG_ip_peakdetector_0_reg0
  create_bd_addr_seg -range 0x00010000 -offset 0x44A40000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs dpp_0/pha/pur/ip_peakdetector_0/axibusdomain_s_axi/reg0] SEG_ip_peakdetector_0_reg01
  create_bd_addr_seg -range 0x00010000 -offset 0x44A50000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs dpp_0/pha/pur/ip_pur_0/S00_AXI/S00_AXI_reg] SEG_ip_pur_0_S00_AXI_reg
  create_bd_addr_seg -range 0x00010000 -offset 0x44A10000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs dpp_0/scope/ip_scope_0/axibusdomain_s_axi/reg0] SEG_ip_scope_0_reg0
  create_bd_addr_seg -range 0x00010000 -offset 0x44A60000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs dpp_0/pulse_conditioning_slow/ip_shaper_0/axi_clk_domain_s_axi/reg0] SEG_ip_shaper_0_reg0
  create_bd_addr_seg -range 0x00010000 -offset 0x44AA0000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs dpp_0/pulse_conditioning_fast/ip_shaper_0/axi_clk_domain_s_axi/reg0] SEG_ip_shaper_0_reg01
  create_bd_addr_seg -range 0x00010000 -offset 0x44A30000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs dpp_0/pha/ip_timers_0/S00_AXI/S00_AXI_reg] SEG_ip_timers_0_S00_AXI_reg


  # Restore current instance
  current_bd_instance $oldCurInst

  validate_bd_design
  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


