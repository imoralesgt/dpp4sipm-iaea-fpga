set_property PACKAGE_PIN U2 [get_ports {ads4149_in_data_p[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ads4149_in_data_p[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {ads4149_in_data_p[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {ads4149_in_data_p[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {ads4149_in_data_p[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {ads4149_in_data_p[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {ads4149_in_data_p[5]}]
set_property IOSTANDARD LVDS_25 [get_ports {ads4149_in_data_p[6]}]
set_property PACKAGE_PIN R2 [get_ports {ads4149_in_data_p[1]}]
set_property PACKAGE_PIN N1 [get_ports {ads4149_in_data_p[2]}]
set_property PACKAGE_PIN M2 [get_ports {ads4149_in_data_p[3]}]
set_property PACKAGE_PIN L4 [get_ports {ads4149_in_data_p[4]}]
set_property PACKAGE_PIN K2 [get_ports {ads4149_in_data_p[5]}]
set_property PACKAGE_PIN J5 [get_ports {ads4149_in_data_p[6]}]
set_property PACKAGE_PIN R3 [get_ports ads4149_in_clk_p]

set_property DIFF_TERM TRUE [get_ports {ads4149_in_data_p[6]}]
set_property DIFF_TERM TRUE [get_ports {ads4149_in_data_n[6]}]
set_property DIFF_TERM TRUE [get_ports {ads4149_in_data_p[5]}]
set_property DIFF_TERM TRUE [get_ports {ads4149_in_data_n[5]}]
set_property DIFF_TERM TRUE [get_ports {ads4149_in_data_p[4]}]
set_property DIFF_TERM TRUE [get_ports {ads4149_in_data_n[4]}]
set_property DIFF_TERM TRUE [get_ports {ads4149_in_data_p[3]}]
set_property DIFF_TERM TRUE [get_ports {ads4149_in_data_n[3]}]
set_property DIFF_TERM TRUE [get_ports {ads4149_in_data_p[2]}]
set_property DIFF_TERM TRUE [get_ports {ads4149_in_data_n[2]}]
set_property DIFF_TERM TRUE [get_ports {ads4149_in_data_p[1]}]
set_property DIFF_TERM TRUE [get_ports {ads4149_in_data_n[1]}]
set_property DIFF_TERM TRUE [get_ports {ads4149_in_data_p[0]}]
set_property DIFF_TERM TRUE [get_ports {ads4149_in_data_n[0]}]


#FPGA_NSS
#set_property IOSTANDARD LVCMOS25 [get_ports {stm32_spi_ss_i}]

#FPGA_SCK
#set_property IOSTANDARD LVCMOS25 [get_ports {stm32_spi_sck_i}]

#FPGA_MOSI
#set_property IOSTANDARD LVCMOS25 [get_ports {stm32_spi_io0_i}]

#FPGA_MISO
#set_property IOSTANDARD LVCMOS25 [get_ports {stm32_spi_io1_o}]




set_property PACKAGE_PIN M17 [get_ports {stm32_spi_ss_i}]
set_property PACKAGE_PIN N16 [get_ports {stm32_spi_sck_i}]
set_property PACKAGE_PIN U17 [get_ports {stm32_spi_io0_i}]
set_property PACKAGE_PIN U16 [get_ports {stm32_spi_io1_o}]


set_property IOSTANDARD LVCMOS33 [get_ports stm32_spi_ss_i]
set_property IOSTANDARD LVCMOS33 [get_ports stm32_spi_sck_i]
set_property IOSTANDARD LVCMOS33 [get_ports stm32_spi_io0_i]
set_property IOSTANDARD LVCMOS33 [get_ports stm32_spi_io1_o]


set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk]
