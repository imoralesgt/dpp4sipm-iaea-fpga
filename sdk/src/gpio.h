/*
 * gpio.h
 *
 *  Created on: Mar 31, 2021
 *      Author: Mladen
 */
#include "xparameters.h"
#include "xgpio.h"

#ifndef SRC_GPIO_H_
#define SRC_GPIO_H_

int Gpio_Init(XGpio *GpioInstancePtr, u16 DeviceId);
int Gpio_BlinkLed(XGpio *GpioInstancePtr, int nBlinkCnt);
void Gpio_AdcReset(XGpio *GpioInstancePtr);
void Gpio_AdcOutputEnable(XGpio *GpioInstancePtr);

#endif /* SRC_GPIO_H_ */
