/*
 * dpp.h
 *
 *  Created on: Mar 31, 2021
 *      Author: Mladen
 */

#ifndef SRC_DPP_H_
#define SRC_DPP_H_

void InitSoPC();

int GetCommandId(u8 *u8Command, unsigned int uiCommandSize);
int GetCommandParameter(u8 *u8Command, unsigned int uiCommandSize, int iParameterId, char *szParameter);
int GetCommandParamaterArray(u8 *u8Command, unsigned int uiCommandSize, u32 *prmArray, int prmCnt);

void SendError(int iError);
void DoHostTasks();
void DoCommand001(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand002(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand003(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand004(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand005(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand006(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand007(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand008(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand009(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand010(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand011(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand012(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand013(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand014(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand015(u8 *u8Command, unsigned int uiCommandSize);

//
// variables defined in this module
//
u8 u8Command[MEMORY_RX_BUFFER_SIZE];
u8 u8Replay[UART_TX_BUFFER_SIZE];

char *m_szCmdId[CMD_CNT]={"$FS", "$RS", "$SP", "$LS", "$FM", "$RM", "$AQ", "$WT", "$RT", "$CS","$GP","$GR","$SR","$VR","$RL" };

ip_scope IpScope;
ip_shaper IpShaperSlow;
ip_peakdetector IpPeakDetectorSlow;
ip_shaper IpShaperFast;
ip_peakdetector IpPeakDetectorFast;
XBram Bram;

void scope_SetDefaultUserParameters(u32 *prm);
void shaper_SetDefaultUserParametersSlow (u32 *prm);
void peakdetector_SetDefaultUserParametersSlow(u32 *prm);
void ClearPlSpectrum(u32  base_addr);
void scope_SetDefaultUserParameters(u32 *prm);
void shaper_SetDefaultUserParametersFast(u32 *prm);
void peakdetector_SetDefaultUserParametersFast(u32 *prm);

#define PRM_TABLE_START_SHAPER_SLOW 	0
#define PRM_TABLE_END_SHAPER_SLOW 		11
#define PRM_TABLE_CNT_SHAPER_SLOW 		12

#define PRM_TABLE_START_PKD_SLOW 		12
#define PRM_TABLE_END_PKD_SLOW 			17
#define PRM_TABLE_CNT_PKD_SLOW 			6

#define PRM_TABLE_START_SCOPE 			18
#define PRM_TABLE_END_SCOPE 			24
#define PRM_TABLE_CNT_SCOPE 			7

#define PRM_TABLE_START_TIMERS 			25
#define PRM_TABLE_END_TIMERS 			26
#define PRM_TABLE_CNT_TIMERS 			2

#define PRM_TABLE_START_BLR_SLOW 		27
#define PRM_TABLE_END_BLR_SLOW 			29
#define PRM_TABLE_CNT_BLR_SLOW 			3

#define PRM_TABLE_START_SCOPE_MUX 		30
#define PRM_TABLE_END_SCOPE_MUX 		30
#define PRM_TABLE_CNT_SCOPE_MUX 		1

#define PRM_TABLE_START_DCS_SLOW 		31
#define PRM_TABLE_END_DCS_SLOW 			33
#define PRM_TABLE_CNT_DCS_SLOW 			3

#define PRM_TABLE_START_INVERT_OFFSET 	34
#define PRM_TABLE_END_INVERT_OFFSET 	34
#define PRM_TABLE_CNT_INVERT_OFFSET 	1


#define PRM_TABLE_START_SHAPER_FAST 	35
#define PRM_TABLE_END_SHAPER_FAST 		46
#define PRM_TABLE_CNT_SHAPER_FAST 		12

#define PRM_TABLE_START_BLR_FAST 		47
#define PRM_TABLE_END_BLR_FAST 			49
#define PRM_TABLE_CNT_BLR_FAST 			3

#define PRM_TABLE_START_DCS_FAST 		50
#define PRM_TABLE_END_DCS_FAST 			52
#define PRM_TABLE_CNT_DCS_FAST 			3

#define PRM_TABLE_START_PKD_FAST 		53
#define PRM_TABLE_END_PKD_FAST 			58
#define PRM_TABLE_CNT_PKD_FAST 			6

#define PRM_TABLE_START_PUR 			59
#define PRM_TABLE_END_PUR 				61
#define PRM_TABLE_CNT_PUR 				3

u32 prm_table[] =
{
	////////////////////////
	// Shaper slow
	// $SP 1
	////////////////////////
	20,			//Tclk: 			clock period time in nsec
	80,		//Taur:					rise time of exponential input signal in nsec
	250,		//Taud:				decay time of exponential input signal in nsec
	1000,		//Taupk:			trapezoid filter: peaking time in nsec
	100,		//Taupk_top:		trapezoid flat top in nsec
	33554432,	//gain;				gain at output of the filter
	15487322, 	//b10: 				(exp(-Tclk/Taud,24,24);
	671088,		//na_inv			(Tclk/Taupk,26,25)
	47,			//na				Taupk/Tclk (10.0)
	52,			//nb				(Taupk+taupk_top)/Tclk (10.0)
	46242897,	//U(1,32,24);		b00
	13066108,	//U(0,24,24);		b20

	////////////////////////
	// Peak detector slow
	// $SP 2
	////////////////////////
	20,			//Tclk: 			clock period time in nsec
	30,			//x_delay 			= (int) ((0.1*IpShaper.ConfigPtr->Taupk+IpShaper.ConfigPtr->Taupk_top)/(double)IpShaper.ConfigPtr->Tclk);		//trapezoid filter: peaking time in sec
	81,			//x_noise 			= (0.005,16,14);;
	131,		//x_min 			= (0.01,16,14);
	32602,		//x_max 			= (1.99,16,14);
	1,			//en_pkd 			= 1;

	////////////////////////
	// Scope
	// $SP 3
	////////////////////////
	20,			//Tclk: 			clock period time in nsec
	2048,		//bram_size:		scope size
	1638,		//(-1,16,14);		threshold 1638 == 0.1, 49152 == -1
	100,		//delay				delay in clocks
	1,			//enable			enable
	1,			//clear
	1,			//full

	////////////////////////
	// Timers
	// $SP4
	////////////////////////
	10000000,	//preset			register 4
	176,		//

	////////////////////////
	// BLR slow
	// $SP 5
	////////////////////////
	245, 		//threshold high
	64881,		//threshold low
	4,			//enable, shift		bit 5 = enable (bit 4, bit 3, bit 2, bit 1) - shift

	////////////////////////
	// Scope mux
	// $SP 6
	////////////////////////
	47,

	////////////////////////
	// DCS slow
	// $SP 7
	////////////////////////
	245, 		//threshold high
	64717,		//threshold low
	47,			//enable, shift		bit 6 = enable (bit 5, bit 4, bit 3, bit 2, bit 1) - shift

	////////////////////////
	// Invert & offset
	// $SP 8
	////////////////////////
	25478, 		//offset, ivert		offset = bit 1 to 14, invert = bit 15

	////////////////////////
	// Shaper fast
	// $SP 9
	////////////////////////
	20,			//Tclk: 			clock period time in nsec
	80,			//Taur:				rise time of exponential input signal in nsec
	250,		//Taud:				decay time of exponential input signal in nsec
	200,		//Taupk:			trapezoid filter: peaking time in nsec
	0,			//Taupk_top:		trapezoid flat top in nsec
	33554432,	//gain;				gain at output of the filter
	15487322, 	//b10: 				(exp(-Tclk/Taud,24,24);
	3355443,	//na_inv			(Tclk/Taupk,26,25)
	7,			//na				Taupk/Tclk (10.0)
	7,			//nb				(Taupk+taupk_top)/Tclk (10.0)
	46242897,	//U(1,32,24);		b00
	13066108,	//U(0,24,24);		b20

	////////////////////////
	// BLR fast
	// $SP 10
	////////////////////////
	327, 		//threshold high
	64881,		//threshold low
	4,			//enable, shift		bit 5 = enable (bit 4, bit 3, bit 2, bit 1) - shift

	////////////////////////
	// DCS fast
	// $SP 11
	////////////////////////
	245, 		//threshold high
	64717,		//threshold low
	47,			//enable, shift		bit 6 = enable (bit 5, bit 4, bit 3, bit 2, bit 1) - shift

	////////////////////////
	// Peak detector fast
	// $SP 12
	////////////////////////
	20,			//Tclk: 			clock period time in nsec
	5,			//x_delay 			(int) ((0.1*IpShaper.ConfigPtr->Taupk+IpShaper.ConfigPtr->Taupk_top)/(double)IpShaper.ConfigPtr->Tclk);		//trapezoid filter: peaking time in sec
	196,		//x_noise 			= (0.005,16,14);;
	245,		//x_min 			= (0.01,16,14);
	32602,		//x_max 			= (1.99,16,14);
	1,			//en_pkd 			= 1;

	////////////////////////
	// PUR
	// $SP 13
	////////////////////////
	80,
	1,
	12

};

#endif /* SRC_DPP_H_ */

