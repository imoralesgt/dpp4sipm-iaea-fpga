/*
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A 
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR 
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION 
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE 
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO 
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO 
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE 
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * 
 *
 * This file is a generated sample test application.
 *
 * This application is intended to test and/or illustrate some 
 * functionality of your system.  The contents of this file may
 * vary depending on the IP in your system and may use existing
 * IP driver functions.  These drivers will be generated in your
 * SDK application project when you run the "Generate Libraries" menu item.
 *
 */

#include <stdio.h>
#include "xparameters.h"
#include "xil_cache.h"
#include "xgpio.h"
//#include "gpio_header.h"
#include "xspi.h"
#include "define.h"

#include "ads4149.h"
#include "gpio.h"
#include "uart.h"
#include "ip_scope.h"
#include "invert_and_offset.h"
#include "ip_mux16_2_if.h"
#include "ip_shaper.h"
#include "ip_dc_stabilizer.h"
#include "ip_peakdetector.h"
#include "ip_timers.h"
#include "ip_pur.h"
#include "shiftregistercontrol.h"
#include "xbram.h"

#include "intc.h"
#include "registers.h"
#include "util.h"

#include "dpp.h"

static XSpi  SpiInstance;	 /* The instance of the SPI device */
static XGpio GpioInstance_0;
static XGpio GpioInstance_1;

int main ()
{
	Xil_ICacheEnable();
	Xil_DCacheEnable();
//	print("---Entering main---\n\r"); //IM Removed welcome message via UART

	InitSoPC();

	Gpio_BlinkLed(&GpioInstance_0, 3);
	while(1)
	{
	   DoHostTasks();
	}

	print("---Exiting main---\n\r");
	Xil_DCacheDisable();
	Xil_ICacheDisable();
	return 0;
}

void InitSoPC()
{
	XBram_Config *XBram_ConfigPtr;
	u32 u32Data;

	//**************************************************
	//	initialize GPIO controller
	//**************************************************
	Gpio_Init(&GpioInstance_0,XPAR_GPIO_0_DEVICE_ID);
	Gpio_Init(&GpioInstance_1,XPAR_GPIO_1_DEVICE_ID);
	Gpio_AdcReset(&GpioInstance_1);
	//**************************************************
	//	initialize UART controller
	//**************************************************
	UartLiteInit(XPAR_UARTLITE_0_DEVICE_ID);
	//**************************************************
	//	initialize AXI bus SPI controller
	//**************************************************
	Ads4149_InitSpi(&SpiInstance,XPAR_SPI_0_DEVICE_ID);
	//**************************************************
	//	initialize ADC
	//**************************************************
	//Ads4149_ResetAdc(&SpiInstance);
	//Ads4149_SetPattern(&SpiInstance);
	Ads4149_WriteRegister(&SpiInstance,ADS4149_ADDR_REG_PATTERN,0x0);//0);//0x4);
	Ads4149_WriteRegister(&SpiInstance,ADS4149_ADDR_REG_DATA_FORMAT,0x80);
	Ads4149_WriteRegister(&SpiInstance,ADS4149_ADDR_REG_LOW_SPEED_MODE,ADS4149_LOW_SPEED_MODE_ENABLED);
	Ads4149_WriteRegister(&SpiInstance,ADS4149_ADDR_REG_DATA_STANDARD, ADS4149_DATA_STANDARD_DDR_LVDS);
	Ads4149_WriteRegister(&SpiInstance,ADS4149_ADDR_REG_DIS_LOW_LATENCY, ADS4149_DIS_LOW_LATENCY);
	Ads4149_WriteRegister(&SpiInstance,ADS4149_ADDR_REG_HIGH_PERFORMANCE_MODE, ADS4149_BEST_PERFORMANCE_MODE);

	Ads4149_SetReadWriteMode(&SpiInstance,ADS4149_READ_MODE);
	Ads4149_ReadRegister(&SpiInstance,ADS4149_ADDR_REG_PATTERN);
	Ads4149_ReadRegister(&SpiInstance,ADS4149_ADDR_REG_DATA_FORMAT);
	Ads4149_ReadRegister(&SpiInstance,ADS4149_ADDR_REG_LOW_SPEED_MODE);
	Ads4149_ReadRegister(&SpiInstance,ADS4149_ADDR_REG_DATA_STANDARD);
	Ads4149_ReadRegister(&SpiInstance,ADS4149_ADDR_REG_HIGH_PERFORMANCE_MODE);

	//**************************************************
	//	initialize invert and offset
	//**************************************************
	Xil_Out32(INVERT_OFFSET_REG0, prm_table[PRM_TABLE_START_INVERT_OFFSET]);

	//**************************************************
	//	initialize IP Scope
	//**************************************************
	ip_scope_Initialize(&IpScope, XPAR_IP_SCOPE_0_DEVICE_ID);
	memcpy(IpScope.Config.prm, &prm_table[PRM_TABLE_START_SCOPE], PRM_TABLE_CNT_SCOPE<<2);
	ip_scope_WriteLogic(&IpScope);
	ip_scope_Acq(&IpScope,1);

	//**************************************************
	//	initialize IP Shaper Slow
	//**************************************************
	ip_shaper_Initialize(&IpShaperSlow, XPAR_DPP_0_PULSE_CONDITIONING_SLOW_IP_SHAPER_0_DEVICE_ID);
	memcpy(IpShaperSlow.Config.prm, &prm_table[PRM_TABLE_START_SHAPER_SLOW], PRM_TABLE_CNT_SHAPER_SLOW<<2);
	ip_shaper_WriteLogic(&IpShaperSlow);

	//**************************************************
	//	initialize IP Peak Detector Slow
	//**************************************************
	ip_peakdetector_Initialize(&IpPeakDetectorSlow, XPAR_DPP_0_PHA_PKD_IP_PEAKDETECTOR_0_DEVICE_ID);
	memcpy(IpPeakDetectorSlow.Config.prm, &prm_table[PRM_TABLE_START_PKD_SLOW], PRM_TABLE_CNT_PKD_SLOW<<2);
	ip_peakdetector_WriteLogic(&IpPeakDetectorSlow);

	//**************************************************
	//	initialize IP BRAM Controller
	//**************************************************
	XBram_ConfigPtr = XBram_LookupConfig(XPAR_BRAM_0_DEVICE_ID);
	XBram_CfgInitialize(&Bram, XBram_ConfigPtr, XBram_ConfigPtr->CtrlBaseAddress);

	//**************************************************
	//	initialize IP Shaper Fast
	//**************************************************
	ip_shaper_Initialize(&IpShaperFast, XPAR_DPP_0_PULSE_CONDITIONING_FAST_IP_SHAPER_0_DEVICE_ID);
	memcpy(IpShaperFast.Config.prm, &prm_table[PRM_TABLE_START_SHAPER_FAST], PRM_TABLE_CNT_SHAPER_FAST<<2);
	ip_shaper_WriteLogic(&IpShaperFast);

	//**************************************************
	//	initialize IP Peak Detector Fast
	//**************************************************
	ip_peakdetector_Initialize(&IpPeakDetectorFast, XPAR_DPP_0_PHA_PUR_IP_PEAKDETECTOR_0_DEVICE_ID);
	memcpy(IpPeakDetectorFast.Config.prm, &prm_table[PRM_TABLE_START_PKD_FAST], PRM_TABLE_CNT_PKD_FAST<<2);
	ip_peakdetector_WriteLogic(&IpPeakDetectorFast);

	//**************************************************
	//	initialize IP Timers
	//**************************************************
	Xil_Out32(TIMER_REGISTER4, prm_table[PRM_TABLE_START_TIMERS+0]);
	Xil_Out32(TIMER_REGISTER5, prm_table[PRM_TABLE_START_TIMERS+1]);

	//**************************************************
	//	initialize DCS Slow
	//**************************************************
	Xil_Out32(SLOWDCS_REG0, prm_table[PRM_TABLE_START_DCS_SLOW+0]);
	Xil_Out32(SLOWDCS_REG1, prm_table[PRM_TABLE_START_DCS_SLOW+1]);
	Xil_Out32(SLOWDCS_REG2, 0); //reset
	Xil_Out32(SLOWDCS_REG2, prm_table[PRM_TABLE_START_DCS_SLOW+2]);

	//**************************************************
	//	initialize DCS Fast
	//**************************************************
	Xil_Out32(FASTDCS_REG0, prm_table[PRM_TABLE_START_DCS_FAST+0]);
	Xil_Out32(FASTDCS_REG1, prm_table[PRM_TABLE_START_DCS_FAST+1]);
	Xil_Out32(FASTDCS_REG2, 0); //reset
	Xil_Out32(FASTDCS_REG2, prm_table[PRM_TABLE_START_DCS_FAST+2]);

	//**************************************************
	//	initialize PUR
	//**************************************************
	Xil_Out32(PUR_REG0, prm_table[PRM_TABLE_START_PUR+0]);
	Xil_Out32(PUR_REG1, prm_table[PRM_TABLE_START_PUR+1]);
	Xil_Out32(SHIFT_REG0, prm_table[PRM_TABLE_START_PUR+2]);

	//**************************************************
	//	initialize interrupt controller
	//**************************************************
	IntcPSInit((u16)XPAR_INTC_0_DEVICE_ID);
	IntcEnableInterrupt(XPAR_INTC_0_DEVICE_ID);

}

void DoHostTasks()
   {
   int cmdID;
   unsigned int uiReceivedCount;

   if(UartLiteReceiveCommand(XPAR_UARTLITE_0_DEVICE_ID, u8Command, &uiReceivedCount, MEMORY_RX_BUFFER_SIZE) == XST_SUCCESS)
      {
      cmdID = GetCommandId(u8Command, uiReceivedCount) + 1;
      switch (cmdID)
         {
         //"$SP": Set Filter parameters
         case 3:
            DoCommand003(u8Command, uiReceivedCount);
			break;
         //"$LS" Load Scope ( PS Scope -> Host; PS Scope is flushed automatically in interrupt mode)
         case 4:
            DoCommand004(u8Command, uiReceivedCount);
			break;
		 //"$RM": Read spectrum ( PS DRAM - host)
		 case 6:
			DoCommand006(u8Command, uiReceivedCount);
			break;
		 //"$AQ": AcQuisition start/stop
		 case 7:
			DoCommand007(u8Command, uiReceivedCount);
			break;
		 //"$WT": Write Timer register
		 case 8:
			DoCommand008(u8Command, uiReceivedCount);
			break;
		 //"$RT": Read Timer register
		 case 9:
			DoCommand009(u8Command, uiReceivedCount);
			break;
		 //"$CS": Clear Spectrum
		 case 10:
			DoCommand010(u8Command, uiReceivedCount);
			break;
		 //"$GP": Get parameters
		 case 11:
			DoCommand011(u8Command, uiReceivedCount);
			break;
		 //"$GR": Get ROI
		 case 12:
			DoCommand012(u8Command, uiReceivedCount);
			break;
		 //"$SR": Set ROI
		 case 13:
			DoCommand013(u8Command, uiReceivedCount);
			break;
         //"$VR": Read ROI values
         case 14:
            DoCommand014(u8Command, uiReceivedCount);
            break;
         case 15://$RL: get RealTimer/LiveTimer
        	DoCommand015(u8Command, uiReceivedCount);
        	break;
         default:
			SendError(ERROR_INVALID_COMMAND);
			break;
		 }
      }
   }

int GetCommandId(u8 *u8Command, unsigned int uiCommandSize)
   {
   int i;
   char szCmdId[4];

   for(i=0;i<3;i++) szCmdId[i] = u8Command[i];
   szCmdId[3]='\0';

   for(i=0;i<CMD_CNT;i++)
	  {
	  if(strcmp(szCmdId, m_szCmdId[i]) == 0)return (i);
	  }
   return -1;
   }


//
// CMD003: $SP = Set parameters
// command parameters: param_group_id, p1, p2, ... pn
//
void DoCommand003(u8 *u8Command, unsigned int uiCommandSize)
	{
	int iParam;
	char szParameter[32];
	u32 u32Data;
	if( GetCommandParameter(u8Command,uiCommandSize,1,szParameter) == 0) goto failed;
	iParam = atoi32(szParameter);
	switch(iParam)
	{
		case 1://IP_Shaper parameter group
		{
			if(GetCommandParamaterArray(u8Command, uiCommandSize, IpShaperSlow.Config.prm, IP_SHAPER_PRM_CNT) == 0) goto failed;
			ip_shaper_WriteLogic(&IpShaperSlow);
			//dependent parameters: IP PeakDetector: x_delay
			break;
		}
		case 2: //IP Peak Detector parameter group
			if(GetCommandParamaterArray(u8Command, uiCommandSize, IpPeakDetectorSlow.Config.prm, IP_PEAKDETECTOR_PRM_CNT) == 0) goto failed;
			ip_peakdetector_WriteLogic(&IpPeakDetectorSlow);
			break;
		case 3://IP Scope parameter group
			if(GetCommandParamaterArray(u8Command, uiCommandSize, IpScope.Config.prm, IP_SCOPE_PRM_CNT) == 0) goto failed;
			ip_scope_WriteLogic(&IpScope);
			break;
		case 4: //IP Timers parameter group
			if( GetCommandParameter(u8Command,uiCommandSize,2,szParameter) == 0) goto failed;
			u32Data = atou32(szParameter);
			Xil_Out32(TIMER_REGISTER4, u32Data);
			if( GetCommandParameter(u8Command,uiCommandSize,3,szParameter) == 0) goto failed;
			u32Data = atou32(szParameter) + (Xil_In32(TIMER_REGISTER5) &0x00000003) ;
			Xil_Out32(TIMER_REGISTER5, u32Data);
			break;
		case 5://BLR Slow parameter group
			break;
		case 6: //Scopemux parameter group
			if( GetCommandParameter(u8Command,uiCommandSize,2,szParameter) == 0) goto failed;
			u32Data = atou32(szParameter);
			Xil_Out32(SCOPEMUX_REG0, u32Data);
			break;
		case 7: //DCS Slow parameter group
			if( GetCommandParameter(u8Command,uiCommandSize,2,szParameter) == 0) goto failed;
			u32Data = atou32(szParameter); //threshold_high
			Xil_Out32(SLOWDCS_REG0, u32Data);
			if( GetCommandParameter(u8Command,uiCommandSize,3,szParameter) == 0) goto failed;
			u32Data = atou32(szParameter); //threshold_low
			Xil_Out32(SLOWDCS_REG1, u32Data);
			if( GetCommandParameter(u8Command,uiCommandSize,4,szParameter) == 0) goto failed;
			Xil_Out32(SLOWDCS_REG2, 0); //reset
			u32Data = atou32(szParameter);
			Xil_Out32(SLOWDCS_REG2, u32Data);
			break;
		case 8: //invert & offset parameter group
			if( GetCommandParameter(u8Command,uiCommandSize,2,szParameter) == 0) goto failed;
			u32Data = atou32(szParameter);
			Xil_Out32(INVERT_OFFSET_REG0, u32Data);
			break;
		case 9://Shaper Fast parameters
			 if(GetCommandParamaterArray(u8Command, uiCommandSize, IpShaperFast.Config.prm, IP_SHAPER_PRM_CNT) == 0) goto failed;
			 ip_shaper_WriteLogic(&IpShaperFast);
			 //dependent parameters: IP PeakDetector: x_delay
			 break;
		case 10://BLR Fast parameters
			 break;
		case 11://DCS Fast parameters
			 if( GetCommandParameter(u8Command,uiCommandSize,2,szParameter) == 0) goto failed;
			 u32Data = atou32(szParameter);
			 Xil_Out32(FASTDCS_REG0, u32Data);
			 if( GetCommandParameter(u8Command,uiCommandSize,3,szParameter) == 0) goto failed;
			 u32Data = atou32(szParameter);
			 Xil_Out32(FASTDCS_REG1, u32Data);
			 if( GetCommandParameter(u8Command,uiCommandSize,4,szParameter) == 0) goto failed;
			 Xil_Out32(FASTDCS_REG2, 0);
			 u32Data = atou32(szParameter);
			 Xil_Out32(FASTDCS_REG2, u32Data);
			 break;
		case 12://Peak Detector Fast parameters
			 if(GetCommandParamaterArray(u8Command, uiCommandSize, IpPeakDetectorFast.Config.prm, IP_PEAKDETECTOR_PRM_CNT) == 0) goto failed;
			 ip_peakdetector_WriteLogic(&IpPeakDetectorFast);
			 break;
		case 13://PileUp Rejector parameters
			 if( GetCommandParameter(u8Command,uiCommandSize,2,szParameter) == 0) goto failed;
			 u32Data = atou32(szParameter);
			 Xil_Out32(PUR_REG0, u32Data);
			 if( GetCommandParameter(u8Command,uiCommandSize,3,szParameter) == 0) goto failed;
			 u32Data = atou32(szParameter);
			 Xil_Out32(PUR_REG1, u32Data);
			 if( GetCommandParameter(u8Command,uiCommandSize,4,szParameter) == 0) goto failed;
			 u32Data = atou32(szParameter);
			 Xil_Out32(SHIFT_REG0, u32Data);
			 break;

		default:
			goto failed;
			break;
	}
	u8Replay[0] = '!';
	u8Replay[1] = 'S';
	u8Replay[2] = 'P';
	u8Replay[3] = '\n';
	u8Replay[4] = '\r';
	UartLiteSendReply(XPAR_MB_PERIPH_0_AXI_UARTLITE_0_DEVICE_ID, u8Replay,5);

	return;
failed:
   SendError(ERROR_INVALID_COMMAND_PARAMETER);
   }

//
// CMD004: $LS = Load data from scope (same as Read but using interrupt)
// command parameter: channel ID (integer value 1 or 2)
//
void DoCommand004(u8 *u8Command, unsigned int uiCommandSize)
   {
   if(IntcIsWaveformReady()==0)
	  {
	  SendError(ERROR_SCOPE_DATA_NOT_READY);
	  return;
	  }

   //header
   u8Replay[0] = '!';
   u8Replay[1] = 'L';
   u8Replay[2] = (u8) '\n';
   u8Replay[3] = (u8) '\r';



   //data
   u32 *data = (u32 *) (u8Replay + 4);
   ip_scope_ReadWaveform(&IpScope,data);

   UartLiteSendReply(XPAR_MB_PERIPH_0_AXI_UARTLITE_0_DEVICE_ID, u8Replay, MEMORY_TX_BUFFER_SIZE);
   u8Replay[0] = (u8) '\n';
   u8Replay[1] = (u8) '\r'; //IM added End of transmission character at the end
   UartLiteSendReply(XPAR_MB_PERIPH_0_AXI_UARTLITE_0_DEVICE_ID, u8Replay, 2);

   //clear scope and enable interrupt
   ip_scope_WaveformAccepted(&IpScope);
   IntcEnableInterrupt( XPAR_PS_MB_0_AXI_INTC_0_DPP_0_SCOPE_IP_SCOPE_0_FULL_INTR);
   }

//
// CMD006: $RM = Get data from spectrum
//
void DoCommand006(u8 *u8Command, unsigned int uiCommandSize)
   {
	char szParameter[32];
	int iParam;
	u32 addr;
	int i;
	//data
	u32 *data = (u32 *) (u8Replay + 4);
	//header
	u8Replay[0] = '!';
	u8Replay[1] = 'R';
	u8Replay[2] = '\n';
	u8Replay[3] = '\r';

	if( GetCommandParameter(u8Command,uiCommandSize,1,szParameter) == 0) goto failed;

	iParam = atoi32(szParameter);
	switch(iParam)
	{
		case 0://read low portion
			for(i=0; i< BRAM_SPECTRUM_SIZE;i++)
			{
				addr = XPAR_BRAM_0_BASEADDR+4*i;
				data[i]= Xil_In32(addr);
			}
			break;
		case 1: //read high portion
			for(i=0; i< BRAM_SPECTRUM_SIZE;i++)
			{
				addr = XPAR_BRAM_0_BASEADDR+4*BRAM_SPECTRUM_SIZE+4*i;
				data[i]= Xil_In32(addr);
			}
			break;
		case 2: //read active portion
			if (  Xil_In32(TIMER_REGISTER3) & BRAM_SEGMENT )
			{
				for(i=0; i< BRAM_SPECTRUM_SIZE;i++)
				{
					addr = XPAR_BRAM_0_BASEADDR+4*BRAM_SPECTRUM_SIZE+4*i;
					data[i]= Xil_In32(addr);
				}
			}
			else
			{
				for(i=0; i< BRAM_SPECTRUM_SIZE;i++)
				{
				addr = XPAR_BRAM_0_BASEADDR+4*i;
				data[i]= Xil_In32(addr);
				}
			}
			break;
		case 3: //read non-active portion
			if (Xil_In32 (TIMER_REGISTER3) & BRAM_SEGMENT )
			{
				for(i=0; i< BRAM_SPECTRUM_SIZE;i++)
				{
					addr = XPAR_BRAM_0_BASEADDR+4*i;
					data[i]= Xil_In32(addr);
				}
			}
			else
			{
				for(i=0; i< BRAM_SPECTRUM_SIZE;i++)
				{
					addr = XPAR_BRAM_0_BASEADDR+4*BRAM_SPECTRUM_SIZE+4*i;
					data[i]= Xil_In32(addr);
				}
			}
			break;
		default:
			goto failed;
			break;
	}
	UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay, MEMORY_TX_BUFFER_SIZE);
	u8Replay[0] = '\n';
	u8Replay[1] = '\r'; //IM Added end of transmission character at the end
	UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay, 2);
	return;
failed:
	SendError(ERROR_INVALID_COMMAND_PARAMETER);
	return;
	}

//
// CMD007: $AQ = Acquisiton start/stop
//
void DoCommand007(u8 *u8Command, unsigned int uiCommandSize)
	{
	char szParameter[32];
	int iParam;
	u32 u32RegData;

	if( GetCommandParameter(u8Command,uiCommandSize,1,szParameter) == 0)
	{
		SendError(ERROR_INVALID_COMMAND_PARAMETER);
		return;
	}

	iParam = atoi32(szParameter);
	switch(iParam)
	{
		//stop acquisition
		case 2:
			//stop scope
			ip_scope_Acq(&IpScope,0);
			IntcDisableInterrupt( XPAR_PS_MB_0_AXI_INTC_0_DPP_0_SCOPE_IP_SCOPE_0_FULL_INTR);

			//stop MCA - its is stopped by setting bit 0 and bit 1 of timers register #5 to 0
			// step 1: read register #5
			u32RegData = Xil_In32(TIMER_REGISTER5);
			// step 2: set b1b0 = 00, only first 11 bits are used
			u32RegData &= ~(TIMER_MANUAL_START | TIMER_AUTO_START);
			Xil_Out32(TIMER_REGISTER5, u32RegData);
			break;

		//start acquisition
		case 1:
			//start scope
			ip_scope_Acq(&IpScope,1);
			ip_scope_WaveformAccepted(&IpScope);
			IntcEnableInterrupt( XPAR_PS_MB_0_AXI_INTC_0_DPP_0_SCOPE_IP_SCOPE_0_FULL_INTR);

			//clean BRAM
			//ClearPlSpectrum(XPAR_BRAM_0_BASEADDR+4*BRAM_SPECTRUM_SIZE);
			//ClearPlSpectrum(XPAR_BRAM_0_BASEADDR);

			//start MCA - its is started by setting bit 1 and bit 2 of timers register #5
			// step 1: read register #5
			u32RegData = Xil_In32(TIMER_REGISTER5);
			// step 2: set b2b1 = 01, only first 11 bits are used
			u32RegData &= ~(TIMER_MANUAL_START | TIMER_AUTO_START);
			u32RegData |= TIMER_MANUAL_START;
			Xil_Out32(TIMER_REGISTER5, u32RegData);
			break;

		//clear all timers
		case 4:
			//set bits 9,10,11 of the register 5 to 1 and then to 0
			//step 1: set bits 9,10,11 to 1
			u32RegData = Xil_In32( TIMER_REGISTER5);
			u32RegData|= (TIMERC_CLEARED |TIMERB_CLEARED | TIMERA_CLEARED);
			Xil_Out32(TIMER_REGISTER5, u32RegData);
			//step 2: set bits 9,10,11 to 0
			u32RegData&= ~(TIMERC_CLEARED |TIMERB_CLEARED | TIMERA_CLEARED);
			Xil_Out32(TIMER_REGISTER5, u32RegData);
			break;

		default:
			goto failed;
			break;
	}

	u8Replay[0] = '!';
	u8Replay[1] = 'A';
	u8Replay[2] = 'Q';
	u8Replay[3] = '\n';
	u8Replay[4] = '\r';

	UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
	return;

failed:
	SendError(ERROR_INVALID_COMMAND_PARAMETER);
   }


//
// CMD008: Write to Timers register
// command parameter1: register ID (zero based index)
// command parameter2: register value
//
void DoCommand008(u8 *u8Command, unsigned int uiCommandSize)
   {
	char szParameter1[32];
	char szParameter2[32];
	u32 u32RegData, u32RegId;

	if( GetCommandParameter(u8Command,uiCommandSize,1,szParameter1) == 0) goto failed;
	if( GetCommandParameter(u8Command,uiCommandSize,2,szParameter2) == 0) goto failed;

	u32RegId = atou32(szParameter1);
	u32RegData = atou32(szParameter2);

	Xil_Out32(XPAR_DPP_0_PHA_IP_TIMERS_0_S00_AXI_BASEADDR + (u32RegId<<2), u32RegData);

	//send response
	u8Replay[0] = '!';
	u8Replay[1] = 'W';
	u8Replay[2] = 'T';
	u8Replay[3] = (u8) '\n';
	u8Replay[4] = (u8) '\r';

	UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
	return;
failed:
	SendError(ERROR_INVALID_COMMAND_PARAMETER);
   }

//
// CMD009: $RT = read from a Timers' register
// command parameter1: register ID (zero based index or -1)
// command parameter2: register value
//
void DoCommand009(u8 *u8Command, unsigned int uiCommandSize)
	{
	char szParameter[32];
	int iLen, i32RegId;
	u32 u32RegData, u32RegDataArray[6];
	u32 addr;
	if( GetCommandParameter(u8Command,uiCommandSize,1,szParameter) == 0) goto failed;
	i32RegId = atoi32(szParameter);
	if(i32RegId == -1)    //read all
	{
		addr = TIMER_REGISTER0;
		for(int i=0;i<6;i++)
		{
			u32RegDataArray[i] = Xil_In32(addr);
			addr += 4;
		}
		iLen = u32atoa(u32RegDataArray, 6, (char*)u8Replay+4);
	}
	else    //read one
	{
		addr = TIMER_REGISTER0 + (u32)(i32RegId*4);
		u32RegData = Xil_In32(addr);
		iLen = u32toa(u32RegData, (char*)u8Replay+4);
	}
	//send response: prefix
	u8Replay[0] = '!';
	u8Replay[1] = 'R';
	u8Replay[2] = 'T';
	u8Replay[3] = ' ';
	//send response: sufix
	u8Replay[iLen+4] = '\n';
	u8Replay[iLen+5] = '\r';
	u8Replay[iLen+6] = '\0';
	iLen = strlen((char*)u8Replay);
	UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay, iLen);
	return;
failed:
	SendError(ERROR_INVALID_COMMAND_PARAMETER);
	return;
   }

//
// CMD010: $CS = Clear Spectrum, alternative $AQ 3
// command parameter1: register ID (zero based index)
// command parameter2: register value
//
void DoCommand010(u8 *u8Command, unsigned int uiCommandSize)
	{
	char szParameter[32];
	int iParam;
	if( GetCommandParameter(u8Command,uiCommandSize,1,szParameter) == 0) goto failed;
	iParam = atoi32(szParameter);
	switch(iParam)
	{
		case 0://erase low segment
			ClearPlSpectrum(XPAR_BRAM_0_BASEADDR);
			break;
		case 1: //erase high segment
			ClearPlSpectrum(XPAR_BRAM_0_BASEADDR+4*BRAM_SPECTRUM_SIZE);
			break;
		case 2: //erase active segment
			if (Xil_In32(TIMER_REGISTER3) & BRAM_SEGMENT)
				ClearPlSpectrum(XPAR_BRAM_0_BASEADDR+4*BRAM_SPECTRUM_SIZE);
			else
				ClearPlSpectrum(XPAR_BRAM_0_BASEADDR);
			break;
		case 3: //erase non-active segment
			if (Xil_In32(TIMER_REGISTER3) & BRAM_SEGMENT)
				ClearPlSpectrum(XPAR_BRAM_0_BASEADDR);
			else
				ClearPlSpectrum(XPAR_BRAM_0_BASEADDR+4*BRAM_SPECTRUM_SIZE);
			break;
		default:
			goto failed;
			break;
	}
	u8Replay[0] = '!';
	u8Replay[1] = 'C';
	u8Replay[2] = 'S';
	u8Replay[3] = '\n';
	u8Replay[4] = '\r';
	UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
	return;
failed:
	SendError(ERROR_INVALID_COMMAND_PARAMETER);
	return;
	}

//
// CMD011: $GP = Get parameters
// command parameters: param_group_id, p1, p2, ... pn
//
void DoCommand011(u8 *u8Command, unsigned int uiCommandSize)
{
	int iParam, iLen;
	char szParameter[32];

	if( GetCommandParameter(u8Command,uiCommandSize,1,szParameter) == 0) goto failed;

	iParam = atoi32(szParameter);
	switch(iParam)
		{
		case 1: //Shaper parameters
		{
			u32 *prm = IpShaperSlow.Config.prm;
			iLen = u32atoa(prm, IP_SHAPER_PRM_CNT, (char*)u8Replay+4);
		}
		break;
		case 2: //Peak Detector Slow parameters
		{
			u32 *prm = IpPeakDetectorSlow.Config.prm;
			iLen = u32atoa(prm, IP_PEAKDETECTOR_PRM_CNT, (char*)u8Replay+4);
		}
		break;
		case 3: //Scope parameters
		{
			u32 *prm = IpScope.Config.prm;
			iLen = u32atoa(prm, IP_SCOPE_PRM_CNT, (char*)u8Replay+4);
		}
		break;
		case 4: //Timers parameters
		{
			u32 u32RegDataArray[6];
			u32 addr = TIMER_REGISTER0;
			for(int i=0;i<6;i++)
			{
				u32RegDataArray[i] = Xil_In32(addr);
				addr += 4;
			}
			iLen = u32atoa(u32RegDataArray, 6, (char*)u8Replay+4);
		}
		break;
		case 5: //BLR parameters
		{
			iLen = 0;
		}
		break;
		case 6: //scopeMUX parameters
		{
			u32 u32RegData;
			u32RegData = Xil_In32(SCOPEMUX_REG0);
			iLen = u32atoa(&u32RegData, 1, (char*)u8Replay+4);
		}
		break;
		case 7: //DCS Slow
		{
			u32 u32RegDataArray[3];
			u32 addr = SLOWDCS_REG0;
			for(int i=0;i<3;i++)
			{
				u32RegDataArray[i] = Xil_In32(addr);
				addr += 4;
			}
			iLen = u32atoa(u32RegDataArray, 3, (char*)u8Replay+4);
		}
		break;
		case 8: //invert_offset
		{
			u32 u32RegData;
			u32RegData = Xil_In32(INVERT_OFFSET_REG0);
			iLen = u32atoa(&u32RegData, 1, (char*)u8Replay+4);
		}
		break;
		case 9: //Shaper Fast parameters
		{
			u32 *prm = IpShaperFast.Config.prm;
			iLen = u32atoa(prm, IP_SHAPER_PRM_CNT, (char*)u8Replay+4);
		}
		break;
		case 10: //BLR Fast parameters
		{
			iLen = 0;
		}

		break;
		case 11: //DCS Fast parameters
		{
			u32 u32RegDataArray[3];
			u32 addr = FASTDCS_REG0;
			for(int i=0;i<3;i++)
			{
				u32RegDataArray[i] = Xil_In32(addr);
				addr += 4;
			}
			iLen = u32atoa(u32RegDataArray, 3, (char*)u8Replay+4);
		}
		break;

		case 12: //IP Peak Detector FAST
		{
			u32 *prm = IpPeakDetectorFast.Config.prm;
			iLen = u32atoa(prm, IP_PEAKDETECTOR_PRM_CNT, (char*)u8Replay+4);
		}
		break;
		case 13: //PUR parameters
		{
			u32 u32RegDataArray[3];

			u32RegDataArray[0] = Xil_In32(  PUR_REG0);
			u32RegDataArray[1] = Xil_In32(  PUR_REG1);
			u32RegDataArray[2] = Xil_In32(SHIFT_REG0);

			iLen = u32atoa(u32RegDataArray, 3, (char*)u8Replay+4);
		}
		break;
		default:
			goto failed;
		break;
	}
	//prefix
	u8Replay[0] = (u8) '!';
	u8Replay[1] = (u8) 'G';
	u8Replay[2] = (u8) 'P';
	u8Replay[3] = (u8) ' ';
	//suffix
	u8Replay[iLen+4] = (u8) '\n';
	u8Replay[iLen+5] = (u8) '\r';
	u8Replay[iLen+6] = (u8) '\0';

	iLen = (int) strlen((char *) u8Replay);
	UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,iLen);
	return;

failed:
	SendError(ERROR_INVALID_COMMAND_PARAMETER);
	return;
   }


//
// CMD012: $GR = Get ROIs
// no parameters, send 5 x 32-bit numbers
//
void DoCommand012(u8 *u8Command, unsigned int uiCommandSize)
   {
   u8Replay[0] = '!';
   u8Replay[1] = 'G';
   u8Replay[2] = 'R';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';

   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   }

//
// CMD013: $SR = Set ROIs
//  no parameters, get 5 x 32-bit numbers
//
void DoCommand013(u8 *u8Command, unsigned int uiCommandSize)
   {
   u8Replay[0] = '!';
   u8Replay[1] = 'S';
   u8Replay[2] = 'R';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';

   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   }

//
// CMD014: $VR = Get Value from ROI FIFO
//  no parameters, get 5 x 32-bit numbers
//
void DoCommand014(u8 *u8Command, unsigned int uiCommandSize)
   {
   u8Replay[0] = '!';
   u8Replay[1] = 'V';
   u8Replay[2] = 'R';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';

   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   }

//$RL. get real/live timer values
void DoCommand015(u8 *u8Command, unsigned int uiCommandSize)
	{
	int iLen;
	u32 u32RegDataArray[2];
	u32 addr;

	addr = TIMER_REGISTER6;
	for(int i=0;i<2;i++)
	{
		u32RegDataArray[i] = Xil_In32(addr);
		addr += 4;
	}
	iLen = u32atoa(u32RegDataArray, 2, (char*)u8Replay+4);

	//send response
	//prefix
	u8Replay[0] = '!';
	u8Replay[1] = 'R';
	u8Replay[2] = 'L';
	u8Replay[3] = ' ';
	//sufix
	u8Replay[iLen+4] = '\n';
	u8Replay[iLen+5] = '\r';
	//u8Replay[iLen+6] = '\0';

	//iLen = strlen((char*)u8Replay);
	UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay, iLen+6);
	}

//
// Parses for command parameter
// if parameter is first then iParametrId = 1
// if parameter is second then iParameter = 2 etc
// on return the szParameter contains the command parameter as string (null terminated)
//
int GetCommandParameter(u8 *u8Command, unsigned int uiCommandSize, int iParameterId, char *szParameter)
   {
   char *c1,*c2,*cc;
   int i=0;
   char szCommand[MEMORY_RX_BUFFER_SIZE];

   memcpy(szCommand,u8Command,uiCommandSize);
   szCommand[uiCommandSize]='\0';

   cc = szCommand;

   //look for i-th occurrences of space character '' i=iParametrId
   while(1)
      {
	  i++;
	  c1 = strchr(cc,' ');
	  if(c1 == NULL) return 0;
	  cc = c1 + 1;
      if(i == iParameterId) break;
	  }

   //look for the next ' '
   c2 = strchr(cc,' ');
   if(c2)
	  {
	  *c2 = '\0';
	  strcpy(szParameter,cc);
	  return 1;
	  }

   //look for the '\n'
   c2 = strchr(cc,'\n');
   if(c2)
	  {
	  *c2 = '\0';
	  strcpy(szParameter,cc);
	  return 1;
	  }
   //look for the '\r'
   c2 = strchr(cc,'\r');
   if(c2)
      {
	  *c2 = '\0';
	  strcpy(szParameter,cc);
	  return 1;
	  }
   return 0;
   }

//
// assuming iError in the interval [0, 99]
//
void SendError(int iErrorId)
   {
   char c1,c2;

   c1 = (char) (0x30 + iErrorId/10);
   c2 = (char) (0x30 + iErrorId%10);

   u8Replay[0] = '!';
   u8Replay[1] = 'E';
   u8Replay[2] = 'R';
   u8Replay[3] = 'R';
   u8Replay[4] = 'O';
   u8Replay[5] = 'R';
   u8Replay[6] = ':';
   u8Replay[7] = c1;
   u8Replay[8] = c2;
   u8Replay[9] = '\n';
   u8Replay[10] = '\r';
   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay, 11);
   }

void ClearPlSpectrum(u32  base_addr)
   {
   u32 addr;
   int i;
   for(i=0; i< BRAM_SPECTRUM_SIZE;i++) //lower half
	  {
	  addr = base_addr+4*i;
	  Xil_Out32(addr,0);
	  }
   }

int GetCommandParamaterArray(u8 *u8Command, unsigned int uiCommandSize, u32 *prmArray, int prmCnt)
{
int j=2;
char szParameter[32];

	for(int i=0;i<prmCnt;i++,j++)
	{
		if( GetCommandParameter(u8Command,uiCommandSize,j,szParameter) == 0) return 0;
		prmArray[i] = atou32(szParameter);
	}
	return 1;
}
