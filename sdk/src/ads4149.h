/*
 * ads4149.h
 *
 *  Created on: Mar 31, 2021
 *      Author: Mladen
 */

#ifndef SRC_ADS4149_H_
#define SRC_ADS4149_H_

int Ads4149_InitSpi(XSpi *SpiInstancePtr, u16 SpiDeviceId);
int Ads4149_ResetAdc(XSpi *SpiInstancePtr);
int Ads4149_ReadRegister(XSpi *SpiInstancePtr, u8 address);
int Ads4149_WriteRegister(XSpi *SpiInstancePtr, u8 address, u8 data);
int Ads4149_SetReadWriteMode(XSpi *SpiInstancePtr, u8 mode);

#define ADS4149_ADDR_REG_PATTERN 0x25
#define ADS4149_ADDR_REG_LOW_SPEED_MODE 0xDF
#define ADS4149_ADDR_REG_DATA_FORMAT 0x3D
#define ADS4149_ADDR_REG_DATA_STANDARD 0x41
#define ADS4149_ADDR_REG_DIS_LOW_LATENCY 0x42
#define ADS4149_ADDR_REG_HIGH_PERFORMANCE_MODE 0x3

#define ADS4149_READ_MODE 0x1
#define ADS4149_WRITE_MODE 0x0
#define ADS4149_LOW_SPEED_MODE_ENABLED 0x30
#define ADS4149_DATA_STANDARD_DDR_LVDS 0x40
#define ADS4149_DIS_LOW_LATENCY 0x8
#define ADS4149_BEST_PERFORMANCE_MODE 0x3

#endif /* SRC_ADS4149_H_ */
