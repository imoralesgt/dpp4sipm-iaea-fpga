/*
 * xspi_ads4149.c
 *
 *  Created on: Mar 31, 2021
 *      Author: Mladen
 */

#include "xparameters.h"	/* XPAR parameters */
#include "xspi.h"		/* SPI device driver */
#include "xspi_l.h"
#include "sleep.h"

#include "ads4149.h"

#define SPI_DEVICE_ID		XPAR_SPI_0_DEVICE_ID
#define BUFFER_SIZE		12
u8 ReadBuffer[BUFFER_SIZE];
u8 WriteBuffer[BUFFER_SIZE];
typedef u8 DataBuffer[BUFFER_SIZE];

int Ads4149_InitSpi(XSpi *SpiInstancePtr, u16 SpiDeviceId)
{
	int Status;
	u32 Count;
	u8 Test;
	XSpi_Config *ConfigPtr;	/* Pointer to Configuration data */

	/*
	 * Initialize the SPI driver so that it is  ready to use.
	 */
	ConfigPtr = XSpi_LookupConfig(SpiDeviceId);
	if (ConfigPtr == NULL) {
		return XST_DEVICE_NOT_FOUND;
	}

	Status = XSpi_CfgInitialize(SpiInstancePtr, ConfigPtr,
				  ConfigPtr->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}


	Status = XSpi_SetOptions(SpiInstancePtr, XSP_MASTER_OPTION | XSP_MANUAL_SSELECT_OPTION | XSP_CLK_ACTIVE_LOW_OPTION);// | XSP_LOOPBACK_OPTION);

	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	//XSpi_Enable(SpiInstancePtr);

	XSpi_Start(SpiInstancePtr);

	XSpi_IntrGlobalDisable(SpiInstancePtr);


	return XST_SUCCESS;

}


int Ads4149_SetPattern(XSpi *SpiInstancePtr)
{
	int Status,i=1;

	Status = XSpi_SetSlaveSelect(SpiInstancePtr, 0x1);
	if(Status != XST_SUCCESS)
	      return XST_FAILURE;

	WriteBuffer[1] = 0x0; //register value (pattern)
	WriteBuffer[0] = 0x25;//register address

	while(i--)
	{
			//usleep(10);
			Status = XSpi_Transfer(SpiInstancePtr, WriteBuffer, NULL, 2);
			//usleep(10);
			//if(Status == XST_SUCCESS) break;
	}

	Status = XSpi_SetSlaveSelect(SpiInstancePtr, 0x0);
	if(Status != XST_SUCCESS)
	      return XST_FAILURE;

	return XST_SUCCESS;
}

int Ads4149_SetReadWriteMode(XSpi *SpiInstancePtr, u8 mode)
{
	int Status,i=1;

	Status = XSpi_SetSlaveSelect(SpiInstancePtr, 0x1);
	if(Status != XST_SUCCESS)
	      return XST_FAILURE;
	usleep(10000);
	WriteBuffer[1] = mode; //register value (READOUT bit) 0x1=READ MODE; 0x0= WRITE MODE
	WriteBuffer[0] = 0x0;//register address
	//write 1 to READOUT beat at address = 0
	i=1;
	while(i--)
	{
			//usleep(10);
			Status = XSpi_Transfer(SpiInstancePtr, WriteBuffer, NULL, 2);
			//usleep(10);
			//if(Status == XST_SUCCESS) break;
	}
	Status = XSpi_SetSlaveSelect(SpiInstancePtr, 0x0);
	if(Status != XST_SUCCESS)
	      return XST_FAILURE;
	usleep(10000);
}

int Ads4149_ReadRegister(XSpi *SpiInstancePtr, u8 address)
{
	int Status,i=1;

	usleep(10000);

	WriteBuffer[1] = 0x0; 		//register value (do not care)
	WriteBuffer[0] = address;  	//register address
	ReadBuffer[1] = 0xFF;
	ReadBuffer[0] = 0xFF;
	Status = XSpi_SetSlaveSelect(SpiInstancePtr, 0x1);
	if(Status != XST_SUCCESS)
	      return XST_FAILURE;
	i=1;
	while(i--)
	{
			//usleep(10);
			Status = XSpi_Transfer(SpiInstancePtr, WriteBuffer, ReadBuffer, 2);
			//usleep(10);
			//if(Status == XST_SUCCESS) break;
	}
	Status = XSpi_SetSlaveSelect(SpiInstancePtr, 0x0);
	if(Status != XST_SUCCESS)
	      return XST_FAILURE;
	usleep(10000);

	return XST_SUCCESS;
}

int Ads4149_WriteRegister(XSpi *SpiInstancePtr, u8 address, u8 data)
{
	int Status,i=1;

	usleep(10000);
	Status = XSpi_SetSlaveSelect(SpiInstancePtr, 0x1);
	if(Status != XST_SUCCESS)
	      return XST_FAILURE;

	WriteBuffer[1] = data; //register value (pattern)
	WriteBuffer[0] = address;//register address

	while(i--)
	{
			//usleep(10);
			Status = XSpi_Transfer(SpiInstancePtr, WriteBuffer, NULL, 2);
			//usleep(10);
			//if(Status == XST_SUCCESS) break;
	}

	Status = XSpi_SetSlaveSelect(SpiInstancePtr, 0x0);
	if(Status != XST_SUCCESS)
	      return XST_FAILURE;
	usleep(10000);

	return XST_SUCCESS;
}
