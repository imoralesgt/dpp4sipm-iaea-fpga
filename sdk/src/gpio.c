/*
 * gpio.c
 *
 *  Created on: Mar 31, 2021
 *      Author: Mladen
 */

#include "xparameters.h"	/* XPAR parameters */
#include "xgpio.h"		/* SPI device driver */
#include "xgpio_l.h"
#include "sleep.h"

#define LED_DELAY	  1000000
#define LED_MAX_BLINK	0x3	/* Number of times the LED Blinks */


int Gpio_Init(XGpio *GpioInstancePtr, u16 DeviceId)
{
	 int Status;

	 /*
	  * Initialize the GPIO driver so that it's ready to use,
	  * specify the device ID that is generated in xparameters.h
	  */
	 Status = XGpio_Initialize(GpioInstancePtr, DeviceId);
	 if (Status != XST_SUCCESS) {
		  return XST_FAILURE;
	 }

	 /* Set the direction for all signals to be outputs */
	 XGpio_SetDataDirection(GpioInstancePtr, 1, 0x0);

	 return XST_SUCCESS;

}


int Gpio_BlinkLed(XGpio *GpioInstancePtr, int nBlinkCnt)
{
	volatile int Delay;
	u32 LedLoop;

	 /* Set the GPIO LED output to low */
	 XGpio_DiscreteClear(GpioInstancePtr, 1, 1);

	for (LedLoop = 0; LedLoop < nBlinkCnt; LedLoop++) {

		/* Set the GPIO Output to High */
		XGpio_DiscreteSet(GpioInstancePtr, 1, 1);

		/* Wait a small amount of time so the LED is visible */
		//for (Delay = 0; Delay < LED_DELAY; Delay++);
		usleep(1000000);

		/* Clear the GPIO Output */
		XGpio_DiscreteClear(GpioInstancePtr, 1, 1);
		/* Wait a small amount of time so the LED is visible */
		//for (Delay = 0; Delay < LED_DELAY; Delay++);
		usleep(1000000);

	 }

	 return XST_SUCCESS;
}

void Gpio_AdcReset(XGpio *GpioInstancePtr)
{
	//ADC_REST is bit b2 (b3 b2 b1 b0) = (TIA_EN, ADC_DRV_EN, ADC_OE, ADC_RESET)
	//clear all 4
	XGpio_DiscreteClear(GpioInstancePtr, 1, 0xF);
	usleep(10000);
	//set all 4
	XGpio_DiscreteSet(GpioInstancePtr, 1, 0xF);
	usleep(1);
	//clear RESET
	XGpio_DiscreteClear(GpioInstancePtr, 1, 0x1);
	usleep(100000);
}

void Gpio_AdcOutputEnable(XGpio *GpioInstancePtr)
{
	//ADC_OE is bit b1 (b3 b2 b1 b0) = (TIA_EN, ADC_DRV_EN, ADC_RESET, ADC_OE, LED)
	XGpio_DiscreteSet(GpioInstancePtr, 1, 0x2);
}
