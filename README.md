# DPP4SiPM - FPGA
## Instructions for project creation from source files

<p>This file will guide you through the process of creating a functional implementation of the DPP4SiPM project using the provided source files (hardware and embedded C application) and a TCL script.</p>

<p><i>Notice: the project was created using Vivado 2019.1. Other software versions may require slight changes in the TCL script.<i></p>

---
### Instructions for hardware project regeneration (Vivado)

+ Create a new directory where your project will be located (aka base directory), and will be simply addressed as **./** in this instrucitons file.
+ Unzip the **board_files.zip** the definition files for the custom board found in the **board** directory. Copy the resulting folder to the Vivado setup path.
+ Unzip the **if** and **ip** files from the **cores** directory to create the **if** and **ip** 
folders respectively within the base directory.
+ Copy the provided source file folders in the base directory, if not already there (**constraints**, **sdk/src**).
You should have a folder structure something like this:
- --> _base directory_
    - --> _board_
    - --> _constraints_
    - --> _cores_
    - --> _if_
    - --> _ip_
    - --> _sdk_

+ Create a new Vivado project withiin the base directory
+ In the Vivado project settings, add the IP Core paths of **ip** and **if** folders
+ Import the constraints file **dpp.xdc** from the provided constraints folder to the project
+ DO NOT create a block design
+ In the Vivado TCL console, make sure you're located at the base directory (making use of the <code>pwd</code> and <code>cd</code> commands)
+ Use the Vivado TCL console to execute the Block Design creation script: 
<pre><code>source ./dpp_bd.tcl </code></pre>
+ If the IP Cores paths were set correctly, a new Block Design with the project description should be shown after a few moments.
+ Create an HDL wrapper for the block design that has just been created
+ Synthesize -> Implement -> Generate Bitstream -> Export Bitstream

---
### Instructions for Embedded C application project (SDK)

Once the bitstream has been exported from Vivado, Launch SDK from the Vivado project
+ In SDK, wait for the BSP to be automatically generated 
+ Create a new Application Project using the exported hardware definitions:
    + Standalone (not FreeRTOS or Linux)
    + Empty application
+ Import the provided source files from **./sdk/src** to the **src** folder of the created application project
+ Compile all (_Ctrl + B_)
+ The project is now ready to run. Program the FPGA, not forgetting to include the built .elf file for the Microblaze.
